![moozumo_logo](https://user-images.githubusercontent.com/31894565/158752176-fd7aaa07-9301-48d7-b90b-0f94cca6d4a7.png)

# Overview

it's a realtime chatting application

# Installation

Make sure you have setup react native environment [here](https://reactnative.dev/docs/getting-started)

Clone this repo

```javascript
$ git clone https://emanu7478@bitbucket.org/rtt-project/moozumo.git
$ cd moozumo
```

Install dependencies

```javascript
$ npm install
```

If you're running iOS, make sure you install the pods

```javascript
$ cd ios
$ pod install
```

Run android or ios

```javascript
$ npx react-native run-android
```

```javascript
$ npx react-native run-ios
```

# Made with help of

• [react-native](https://reactnative.dev/docs/getting-started)  
• [react-navigation](https://reactnavigation.org/docs/getting-started/)  
• [redux](https://redux.js.org/introduction/getting-started)  
• [react-native-gesture-handler](https://docs.swmansion.com/react-native-gesture-handler/docs/installation)  
• [react-native-reanimated](https://docs.swmansion.com/react-native-reanimated/docs)  
• [react-native-screens](https://github.com/software-mansion/react-native-screens)  
• [react-native-async-storage](https://github.com/react-native-async-storage/async-storage)  
• [react-native-callkeep](https://github.com/react-native-webrtc/react-native-callkeep)  
• [react-native-contacts](https://github.com/morenoh149/react-native-contacts)  
• [react-native-fast-image](https://github.com/DylanVann/react-native-fast-image)  
• [react-native-iap](https://react-native-iap.dooboolab.com/docs/intro)  
• [react-native-push-notification](https://github.com/zo0r/react-native-push-notification)  
• [react-native-sqlite-storage](https://github.com/andpor/react-native-sqlite-storage)  
• [socket.io-client](https://github.com/socketio/socket.io-client#readme)  


# File Structure



```
     src
      ├── Assets
           └── Images
      ├── Components
           ├── Call
           ├── Chat
           ├── Contact
           ├── ContactList
           ├── CreateContact
           └── ...
      ├── Config
           └── index.js
      ├── Constants
           ├── colors.js
           ├── countryCodes.json
           ├── typography.json
           └── ...
      ├── Contexts
           └── socket.js
      ├── Helper
           ├── callEvents.tsx
           ├── encryption.js
           ├── fetchAPI.js
           ├── useCallActions.tsx
           ├── useCallEvents.tsx
           ├── useNotificationHandler.tsx
           ├── normalise.ts
           └── ...
      ├── Navigator
           ├── index.js
           └── RootNavigator.js
      ├── Services
           ├── RealTimeEventHandler
           ├── RealTimeEventObserveDecorator
           └── SocketAdapter
      └── Store
           ├── Actions
           ├── Reducers
           ├── Services
           ├── actionTypes.js
           └── reduxSelectors.js
```

# Remote debugging on a real device

1. `adb reverse tcp:8081 tcp:8081`

2.  Open [React Native Debugger](https://github.com/jhen0409/react-native-debugger).

3.  Shake the device & select Debug JS Remotely.

# Generating signed APK

1. Fill up `android/gradle.properties` details.

```
          RELEASE_KEY_ALIAS=*******
          RELEASE_KEY_PASSWORD=*******
          RELEASE_STORE_FILE=*******
          RELEASE_STORE_PASSWORD=*******
```

2. Put `applicationname.keystore` file in `android/app` directory.

3. `cd android`

4. `./gradlew assembleRelease`

5. Find the APK here `android/app/build/outputs/apk/release/app-release.apk`


# Updates

## Play Store / App Store

Major updates are pushed to the App store and Play store.


