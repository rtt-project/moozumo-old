/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Navigator from './src/Navigator';
import {useDispatch, useSelector} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import {getUserInfo, setUserInfo} from './src/Store/Actions/Auth';
import {ASYNC_STORAGE_KEYS, getData, removeFewData, storeData} from './src/Helper/asyncStorage';
import {usePubNub} from 'pubnub-react';
import {getUserInfo as getUserInfoFromState} from './src/Store/reduxSelectors';
import {toggleSnackbar} from './src/Store/Actions/Global';
function App(props) {
  const dispatch = useDispatch();
  const pubnub = usePubNub();
  const userInfo = useSelector(state => getUserInfoFromState(state));

  React.useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.isConnected) _componentDidMount();
      else dispatch(toggleSnackbar('No Internet Available!'));
    });

    return () => unsubscribe();
  }, []);

  const _componentDidMount = async () => {
    const token = await getData(ASYNC_STORAGE_KEYS.authToken);
    if (token) dispatch(getUserInfo(token));
    else dispatch(setUserInfo({}));
  };

  React.useEffect(() => {
    const userLoggedIn = userInfo && userInfo.userId;
    if (userLoggedIn) pubnub.subscribe({channels: [userInfo.userId]});
    return () => {
      pubnub.unsubscribeAll();
    };
  }, [pubnub, userInfo]);

  return <Navigator />;
}

export default App;
