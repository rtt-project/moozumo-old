/**
 * @format
 */
import 'react-native-gesture-handler';

import React from 'react';
import {AppRegistry} from 'react-native';
import PubNub from 'pubnub';
import {PubNubProvider, usePubNub} from 'pubnub-react';

import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import configureStore from './src/Store/configureStore';
import GlobalModal from './src/Components/Common/GlobalModal';
import {getUniqueId, getManufacturer} from 'react-native-device-info';

// const getUniqueId = () => Math.random();

const store = configureStore();

const pubnub = new PubNub({
  publishKey: 'pub-c-0d71f074-d1d2-4f95-9b6d-ec63be23c10f',
  subscribeKey: 'sub-c-fb9acda0-7525-11eb-b7d0-12559890d9d5',
  uuid: getUniqueId(),
  // uuid: 'Pub' + getUniqueId(),
});

const AppWithProvider = () => {
  return (
    <PubNubProvider client={pubnub}>
      <Provider store={store}>
        <App />
        <GlobalModal />
      </Provider>
    </PubNubProvider>
  );
};

AppRegistry.registerComponent(appName, () => AppWithProvider);
