import {RFPercentage} from 'react-native-responsive-fontsize';

const normalize = size => RFPercentage(size);

export const typographyVariant = {
  the25: {
    fontSize: normalize(3),
    fontFamily: 'NotoSans-Regular',
  },
  the16: {
    fontSize: normalize(1.6),
    fontFamily: 'NotoSans-Regular', 
  },
  the17: {
    fontSize: normalize(1.8),
    fontFamily: 'NotoSans-Regular',
  },
  the18: {
    fontSize: normalize(2.0),
    fontFamily: 'NotoSans-Regular',
  },
  the19: {
    fontSize: normalize(2.2),
    fontFamily: 'NotoSans-Regular',
  },
  the12: {
    fontSize: normalize(1.5),
    fontFamily: 'NotoSans-Regular',
  },
  the13: {
    fontSize: normalize(1.5),
    fontFamily: 'NotoSans-Regular',
  },
  the14: {
    fontSize: normalize(1.8),
    fontFamily: 'NotoSans-Regular',
  },
  the15: {
    fontSize: normalize(2),
    fontFamily: 'NotoSans-Regular',
  },
  the20: {
    fontSize: normalize(2.6),
    fontFamily: 'NotoSans-Regular',
  },
  the24: {
    fontSize: normalize(4.4),
    fontFamily: 'NotoSans-Regular',
  },
  the22: {
    fontSize: normalize(3.0),
    fontFamily: 'NotoSans-Regular',
  },
  the30: {
    fontSize: normalize(4.8),
    fontFamily: 'NotoSans-Regular',
  },
  the32: {
    fontSize: normalize(1.8),
    fontFamily: 'NotoSans-Regular',
  },
  the15PinchMore: {
    fontSize: normalize(2.4),
    fontFamily: 'NotoSans-Regular',
  },
  the45: {
    fontSize: normalize(4),
    fontFamily: 'NotoSans-Regular',
  },
  the50: {
    fontSize: normalize(6),
    fontFamily: 'NotoSans-Regular',
  },
  the60: {
    fontSize: normalize(10),
    fontFamily: 'NotoSans-Regular',
  },
};

export const typographyWeight = {
  bold: {
    fontFamily: 'NotoSans-Bold',
  },
  regular: {
    fontFamily: 'NotoSans-Regular',
  },
};
