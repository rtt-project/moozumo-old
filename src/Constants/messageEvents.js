const messageEvents = {
  makeACall: 'incomingCall',
  callCancelled: 'callCancelled',
  callAccepted: 'callAccepted',
  incomingCall: 'incomingCall',
  callHangup: 'callHangup',
};

export default messageEvents;
