export type messagePayload = {
  channelId: string;
  event: string;
  from?: string;
  to?: string;
  metadata: {
    senderInfo: {
      phoneNumber?: string;
      userId?: string;
    };
  };
};

export type senderInfoType = {
  phoneNumber?: string;
  userId?: string;
  name?: string;
};

export type useCallActionsArgumentTypes = [{redirectToChatScreen: (receiverInfo: senderInfoType, isChat?: boolean) => void; callThisUser: (payload: messagePayload, senderInfo: senderInfoType) => void; messageThisUser: (payload: messagePayload) => void; handleCallAction: (callActionType: string, senderInfo: senderInfoType) => void}];
