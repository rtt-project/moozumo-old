import SQLite from 'react-native-sqlite-storage';
import {BaseModel, types} from 'react-native-sqlite-orm';

export default class Contacts extends BaseModel {
  constructor(obj) {
    super(obj);
  }

  static get database() {
    return async () =>
      SQLite.openDatabase({
        name: 'moozumo.db',
        location: 'default',
        createFromLocation: '~moozumo.db',
      });
  }

  static get tableName() {
    return 'contacts';
  }

  static get columnMapping() {
    return {
      id: {type: types.INTEGER, primary_key: true},
      contactId: {type: types.TEXT},
      countryCode: {type: types.TEXT},
      name: {type: types.TEXT},
      email: {type: types.TEXT},
      orderByName: {type: types.TEXT},
      phoneNumber: {type: types.TEXT},
      userId: {type: types.TEXT},
      isPinnedContact: {type: types.BOOLEAN},
      isMoozumoUser: {type: types.BOOLEAN},
    };
  }
}
