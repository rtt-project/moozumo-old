import {Vibration, Platform} from 'react-native';

const ONE_SECOND_IN_MS = 1000;

const PATTERN = [1 * ONE_SECOND_IN_MS, 1 * ONE_SECOND_IN_MS];

const PATTERN_DESC = Platform.OS === 'android' ? 'wait 1s, vibrate 2s, wait 3s' : 'wait 1s, vibrate, wait 2s, vibrate, wait 3s';

export const vibrate = command => {
  if (command === 'start') {
    if (Platform.OS == 'android') Vibration.vibrate(PATTERN, true);
    else Vibration.vibrate();
  } else if (command === 'stop') {
    Vibration.cancel();
  }
};
