import {Alert, Platform} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import DatabaseLayer from 'react-native-sqlite-orm/src/DatabaseLayer';
import {postFetchAPI, API_ROUTES, getFetchAPI} from '../../Helper/fetchAPI';

import {toggleSnackbar} from '../Actions/Global';
import RNFS from 'react-native-fs';
import ContactsORM from './Contacts.ORM';

let db = SQLite.openDatabase(
  {
    name: 'moozumo.db',
    location: 'default',
    createFromLocation: '~moozumo.db',
  },
  () => {},
  error => {},
);

let ExecuteQuery = (sql, params = []) =>
  new Promise((resolve, reject) => {
    db.transaction(trans => {
      trans.executeSql(
        sql,
        params,
        (trans, results) => {
          resolve(results);
        },
        error => {
          reject(error);
        },
      );
    });
  });

export const loadContacts = async pagination => {
  // ORDER BY
  // try {
  //   let contacts = [];
  //   let selectQuery = await ExecuteQuery('SELECT * FROM contacts ORDER BY isPinnedContact DESC,isMoozumoUser DESC,orderByName', []);
  //   let rows = selectQuery.rows.raw();
  //   console.log('rows.length: ', rows.length);
  //   for (let i = 0; i < rows.length; i++) {
  //     let item = rows[i];
  //     Object.keys(item).forEach(Key => {
  //       if (item[Key] === 'null') item[Key] = null;
  //       if (item[Key] === 'true' || item[Key] === 'false') item[Key] = item[Key] === 'true' ? true : false;
  //     });
  //     contacts.push(item);
  //   }
  //   return contacts;
  // } catch (error) {
  //   return [];
  // }

  const options = {
    columns: 'id, name,phoneNumber',
    page: 0,
    limit: 30,
  };

  const response = await ContactsORM.query(options);
  console.log('response: ', response);
  return [];
};
// SELECT COUNT(ProductID) AS NumberOfProducts FROM Products;

export const contactCount = async () => {
  return new Promise(async (resolve, reject) => {
    // let selectQuery = await ExecuteQuery('SELECT COUNT(userId) AS numberOfMoozumoUsers FROM contacts', []);
    // let rows = selectQuery.rows.raw();
    // console.log('rows: ', rows.length);
  });
};

export const insertContacts = async payload => {
  // for (const data of contacts) {
  //   const props = {
  //     id: data.countryCode + data.phoneNumber,
  //     contactId: data.contactId,
  //     countryCode: data.countryCode,
  //     email: data.email,
  //     orderByName: data.name && data.name.toLowerCase(),
  //     phoneNumber: data.phoneNumber,
  //     userId: data.userId,
  //     isPinnedContact: data.isPinnedContact,
  //     isMoozumoUser: data.isMoozumoUser,
  //   };
  //   console.log('props: ', props);
  //   const contact = new ContactsORM(props);
  //   await contact.save();
  // }

  await ContactsORM.createTable();

  const contacts = payload.map(data => ({
    id: data.countryCode + data.phoneNumber,
    name: data.name,
    contactId: data.contactId,
    countryCode: data.countryCode,
    email: data.email,
    orderByName: data.name && data.name.toLowerCase(),
    phoneNumber: data.phoneNumber,
    userId: data.userId,
    isPinnedContact: data.isPinnedContact,
    isMoozumoUser: data.isMoozumoUser,
  }));

  const databaseLayer = new DatabaseLayer(async () => SQLite.openDatabase('moozumo.db'), 'contacts');

  databaseLayer.bulkInsertOrReplace(contacts).then(response => {
    console.log(response);
  });

  // let query;
  // try {
  //   let Table = await ExecuteQuery('CREATE TABLE IF NOT EXISTS contacts (id VARCHAR(100) PRIMARY KEY NOT NULL, contactId VARCHAR(100),isPinnedContact VARCHAR(100),isMoozumoUser VARCHAR(100), countryCode VARCHAR(100), email VARCHAR(100) , name VARCHAR(100), orderByName VARCHAR(150), phoneNumber VARCHAR(100), userId VARCHAR(100))', []);
  //   // let deleteTableData = ExecuteQuery('DELETE FROM contacts', []);
  //   query = 'INSERT OR REPLACE INTO contacts (id,contactId,countryCode, email,name,orderByName,phoneNumber,userId,isPinnedContact,isMoozumoUser) VALUES';
  //   for (let i = 0; i < Data.length; ++i) {
  //     console.log('i: ', i);
  //     query = query + "('" + (Data[i].countryCode + Data[i].phoneNumber) + "','" + (Data[i].contactId || 'null') + "','" + (Data[i].countryCode || '0') + "','" + (Data[i].email || 'null') + "','" + (Data[i].name || 'null') + "','" + (Data[i].name.toLowerCase() || 'null') + "','" + (Data[i].phoneNumber || 'false') + "','" + (Data[i].userId || 'false') + "','" + (Data[i].isPinnedContact || 'false') + "','" + (Data[i].isMoozumoUser || 'false') + "')";
  //     if (i != Data.length - 1) {
  //       query = query;
  //     }
  //     query = query + ';';
  //     ExecuteQuery(query, [])
  //       .then(success => {
  //         console.log('success: ', success);
  //       })
  //       .catch(err => {
  //         if (Platform.OS === 'android') {
  //           var path = RNFS.ExternalDirectoryPath + '/insertContacts' + new Date().getTime() + '.txt';
  //           console.log('error:ExecuteQuery ', err);
  //           RNFS.writeFile(path, `${JSON.stringify(err)},${JSON.stringify(Data[i])},${JSON.stringify(query)}`, 'utf8')
  //             .then(success => {
  //               console.log('success: ', success);
  //             })
  //             .catch(err => {
  //               console.log('writeFile:err: ', err);
  //             });
  //         }
  //       });
  //   }
  //   //
  // } catch (error) {
  //   // dropContacts();
  //   if (Platform.OS === 'android') {
  //     var path = RNFS.ExternalDirectoryPath + '/insertContacts' + new Date().getTime() + '.txt';
  //     console.log('error: ', error);
  //     RNFS.writeFile(path, `${JSON.stringify(error)},${JSON.stringify(Data)},${JSON.stringify(query)}`, 'utf8')
  //       .then(success => {
  //         console.log('success: ', success);
  //       })
  //       .catch(err => {
  //         console.log('writeFile:err: ', err);
  //       });
  //   }
  // }
};

export const dropContacts = async () => {
  try {
    let deleteTableData = await ExecuteQuery('DROP TABLE IF EXISTS contacts', []);
  } catch (err) {}
};

export const searchUser = searchKey => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM contacts  WHERE (name || phoneNumber) LIKE ? LIMIT 0,50', ['%' + searchKey + '%'], (tx, results) => {
        var len = results.rows.length;
        if (len > 0) {
          let res = results.rows.raw();

          res.map(item => {
            Object.keys(item).forEach(Key => {
              if (item[Key] === 'null') item[Key] = null;
              if (item[Key] === 'true' || item[Key] === 'false') item[Key] = item[Key] === 'true' ? true : false;
            });
            return item;
          });
          resolve(res);
        } else {
          resolve([]);
        }
      });
    });
  });
};
