import {Alert, Platform} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import DatabaseLayer from 'react-native-sqlite-orm/src/DatabaseLayer';
import {postFetchAPI, API_ROUTES, getFetchAPI} from '../../Helper/fetchAPI';

import {toggleSnackbar} from '../Actions/Global';
import RNFS from 'react-native-fs';
import ContactsORM from './Contacts.ORM';

let db = SQLite.openDatabase(
  {
    name: 'moozumo.db',
    location: 'default',
    createFromLocation: '~moozumo.db',
  },
  () => {},
  error => {},
);

let ExecuteQuery = (sql, params = []) =>
  new Promise((resolve, reject) => {
    db.transaction(trans => {
      trans.executeSql(
        sql,
        params,
        (trans, results) => {
          resolve(results);
        },
        error => {
          reject(error);
        },
      );
    });
  });

export const loadContacts = async pagination => {
  try {
    const options = {
      columns: 'id,name,phoneNumber,contactId,countryCode,email,userId,isPinnedContact,isMoozumoUser',
      page: pagination.page,
      limit: pagination.limit,
      order: 'isPinnedContact DESC,isMoozumoUser DESC,orderByName',
    };

    const response = await ContactsORM.query(options);

    return response;
  } catch (err) {
    return [];
  }
};
// SELECT COUNT(ProductID) AS NumberOfProducts FROM Products;

export const getTotalContactCount = async () => {
  return new Promise((resolve, reject) => {
    const databaseLayer = new DatabaseLayer(async () => SQLite.openDatabase({name: 'moozumo.db'}));
    databaseLayer
      .executeSql('SELECT COUNT(*) from contacts;')
      .then(response => {
        const count = (response && response.rows && response.rows[0] && response.rows[0]['COUNT(*)']) || 0;
        resolve(count);
      })
      .catch(err => {
        resolve(0);
      });
  });
};
export const getMoozumoUserCount = async () => {
  return new Promise((resolve, reject) => {
    const databaseLayer = new DatabaseLayer(async () => SQLite.openDatabase({name: 'moozumo.db'}));
    databaseLayer
      .executeSql('SELECT COUNT(*) from contacts WHERE isMoozumoUser = true;')
      .then(response => {
        const count = (response && response.rows && response.rows[0] && response.rows[0]['COUNT(*)']) || 0;
        resolve(count);
      })
      .catch(err => {
        resolve(0);
      });
  });
};

export const insertContacts = async payload => {
  return new Promise(async (resolve, reject) => {
    await ContactsORM.createTable();
    const contacts = payload.map(data => ({
      id: data.countryCode + data.phoneNumber,
      name: data.name,
      contactId: data.contactId || null,
      countryCode: data.countryCode,
      email: data.email || null,
      orderByName: data.name && data.name.toLowerCase(),
      phoneNumber: data.phoneNumber,
      userId: data.userId || null,
      isPinnedContact: !!data.isPinnedContact,
      isMoozumoUser: !!data.isMoozumoUser,
    }));
    const databaseLayer = new DatabaseLayer(async () => SQLite.openDatabase('moozumo.db'), 'contacts');
    databaseLayer
      .bulkInsertOrReplace(contacts)
      .then(response => {
        resolve(contacts.length);
      })
      .catch(err => {
        resolve(contacts.length);
        if (Platform.OS === 'android') {
          var path = RNFS.ExternalDirectoryPath + '/insertContacts' + new Date().getTime() + '.txt';

          RNFS.writeFile(path, `${JSON.stringify(err)},${JSON.stringify(contacts)}}`, 'utf8')
            .then(success => {})
            .catch(err => {});
        }
      });
  });
};

export const dropContacts = async () => {
  try {
    let deleteTableData = await ExecuteQuery('DROP TABLE IF EXISTS contacts', []);
  } catch (err) {}
};

export const searchUser = async searchKey => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM contacts  WHERE (name || phoneNumber) LIKE ?', ['%' + searchKey + '%'], (tx, results) => {
        var len = results.rows.length;
        if (len > 0) {
          let res = results.rows.raw();

          res.map(item => {
            Object.keys(item).forEach(Key => {
              if (item[Key] === 'null') item[Key] = null;
              if (item[Key] === 'true' || item[Key] === 'false') item[Key] = item[Key] === 'true' ? true : false;
            });
            return item;
          });
          resolve(res);
        } else {
          resolve([]);
        }
      });
    });
  });
};
