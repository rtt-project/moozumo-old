import * as types from '../actionTypes';

const initialState = {
  modalProps: {
    component: null,
    isVisible: false,
  },
  countryCodes: [],
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case types.TOGGLE_MODAL:
      return {
        ...state,
        modalProps: action.payload,
      };
    case types.SET_COUNTRY_CODES:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default auth;
