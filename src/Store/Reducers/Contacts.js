import * as types from '../actionTypes';

const initialState = {
  contacts: [],
  isContactLoading: true,
  gettingContactsFromDevice: true,
  deviceContacts: [],
  contactsCount: {
    total: 0,
    moozumoUser: 0,
  },
};

const contacts = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER_CONTACTS:
      return {
        ...state,
        contacts: action.data,
        isContactLoading: false,
      };
    case types.SET_DEVICE_CONTACTS:
      return {
        ...state,
        deviceContacts: action.data,
      };
    case types.TOGGLE_CONTACT_FETCH_FROM_DEVICE:
      return {
        ...state,
        deviceContacts: action.data,
        gettingContactsFromDevice: action.data,
      };

    case types.UPDATE_CONTACTS_COUNT:
      return {
        ...state,
        contactsCount: action.data,
      };
    default:
      return state;
  }
};

export default contacts;
