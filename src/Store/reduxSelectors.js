export const getUserInfo = state => state.auth.userInfo;
export const getModalProps = state => state.global.modalProps;
export const getUserFetchState = state => state.auth.userInfoFetching;

export const getUserContactsFromState = state => state.contacts.contacts;
export const contactListLoading = state => state.contacts.isContactLoading;
export const deviceContactLoading = state => state.contacts.gettingContactsFromDevice;
export const getUserDeviceContactsFromState = state => state.contacts.deviceContacts;
export const getContactsPagination = state => state.contacts.pagination;
export const getContactCountsFromState = state => state.contacts.contactsCount;
