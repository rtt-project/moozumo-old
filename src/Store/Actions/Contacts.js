import * as types from '../actionTypes';
import {postFetchAPI, API_ROUTES, getFetchAPI, patchFetchAPI} from '../../Helper/fetchAPI';
import {storeData, ASYNC_STORAGE_KEYS, getData} from '../../Helper/asyncStorage';
import {getContactsPagination, getUserContactsFromState, getUserDeviceContactsFromState, getUserInfo} from '../reduxSelectors';
import {toggleSnackbar} from './Global';
import {request, check, PERMISSIONS, RESULTS} from 'react-native-permissions';
import PhoneNumber from 'awesome-phonenumber';
import Contacts from 'react-native-contacts';
import Modal from 'react-native-modal';
import {Alert, Platform} from 'react-native';
import * as RNLocalize from 'react-native-localize';
import {insertContacts, contactCount, loadContacts, dropContacts, searchUser, getMoozumoUserCount, getTotalContactCount} from '../Services/Contacts';
import validateEmail from '../../Helper/validateEmail';
import ContactsORM from '../Services/Contacts.ORM';
import {logger, fileAsyncTransport} from 'react-native-logs';
import RNFS from 'react-native-fs';

const MAX_CONTACTS_TO_VERIFY = 200;

let ContactInsertionCount = 0;

export const addContactToCloud = (payload, totalContactToInsert) => async (dispatch, getStore) => {
  try {
    const requestBody = {
      contacts: payload,
    };
    const response = await postFetchAPI(API_ROUTES.addContact, requestBody);
    if (response.error) throw response.error;
    const data = response.result;
    return await insertContacts(data.list);
  } catch (error) {
    dispatch(toggleSnackbar(error && error.message));
    return Promise.resolve(error);
  }
};

export const upsertContactsToStore = data => async (dispatch, getStore) => {
  const store = getStore();
  let userContacts = [...getUserContactsFromState(store)];
  userContacts = userContacts.concat(data);
  dispatch(setUserContacts(userContacts));
};

export const storeContactToDevice = data => async (dispatch, getStore) => {
  try {
    let newPerson = {
      emailAddresses: [
        {
          label: 'work',
          email: data.email || '',
        },
      ],
      givenName: data.name,
      displayName: data.name,
      phoneNumbers: [
        {
          label: 'mobile',
          number: `${data.countryCode} ${data.phoneNumber}`,
        },
      ],
    };

    // await Contacts.addContact(newPerson);
    Contacts.openContactForm(newPerson).then(async contact => {
      await dispatch(addContactToCloud([data]));
      dispatch(updateAfterContactsFetchedFromPhoneBook());
      // contact has been saved
    });
  } catch (err) {}
};

export const getContacts = pagination => async dispatch => {
  try {
    dispatch(getTotalAndMoozumoCount());
    await dispatch(getContactsFromLocalDatabase(pagination));
    dispatch(getLatestContactsFromDevice());
  } catch (error) {
    dispatch(toggleSnackbar(error && error.message));
    return Promise.resolve(error);
  }
};

export const getContactsFromCloud = () => async (dispatch, getStore) => {
  try {
    const response = await getFetchAPI(`${API_ROUTES.getUserContacts}?index=1&limit=1000`);
    if (response.error) throw response.error;
    const data = response.result;
    await dropContacts();
    await dispatch(setUserContacts(data.list || []));
  } catch (error) {
    dispatch(toggleSnackbar(error && error.message));
  }
};

export const verifyContacts = async (payload, callback) => {
  var path = RNFS.ExternalDirectoryPath;
  try {
    let totalContacts = [...payload];
    const totalNumberOfBatches = Math.ceil(totalContacts.length / MAX_CONTACTS_TO_VERIFY);
    let i = 0;
    path = path + `/verifyContacts${i}.txt`;
    while (i < totalNumberOfBatches) {
      const start = i * MAX_CONTACTS_TO_VERIFY;
      const end = start + MAX_CONTACTS_TO_VERIFY;
      const contactsToVerify = totalContacts.slice(start, end);
      const requestBody = {
        contacts: contactsToVerify,
      };
      const response = await postFetchAPI(API_ROUTES.verifyContacts, requestBody);
      if (response.error && Platform.OS === 'android') {
        RNFS.writeFile(path, `${JSON.stringify(response.error)},${JSON.stringify(requestBody)}`, 'utf8')
          .then(success => {})
          .catch(err => {});
      } else {
        const data = response.result;
        callback({totalContacts: totalContacts.length, totalNumberOfBatches, start, end, data: generateMissingData(contactsToVerify, data && data.list)});
      }
      i++;
    }
  } catch (err) {
    if (Platform.OS === 'android') {
      RNFS.writeFile(path + '/verifyContactsThrow.txt', `${JSON.stringify(err)}`, 'utf8')
        .then(success => {})
        .catch(err => {});
    }
  }
};

export const generateMissingData = (rawContacts, verifiedContacts) => {
  let moozumoUsers = [];
  let nonMoozumoUsers = [];
  rawContacts.forEach(contact => {
    let contactDetails = verifiedContacts.find(item => item.countryCode === contact.countryCode && item.phoneNumber === contact.phoneNumber);
    let newContact = {...contact, ...(contactDetails || {})};
    if (newContact.isMoozumoUser) moozumoUsers.push(newContact);
    else nonMoozumoUsers.push(newContact);
  });
  return {moozumoUsers, nonMoozumoUsers};
};

export const pinContacts = payload => async dispatch => {
  try {
    const requestBody = {
      contacts: payload,
    };

    const response = await patchFetchAPI(API_ROUTES.pinContacts, requestBody);
    if (response.error) throw response;
    const data = response.result;
    if (data && data.list && data.list.length) {
      await insertContacts(payload);
      dispatch(updateAfterContactsFetchedFromPhoneBook());
      let isPinnedContact = payload && payload[0] && payload[0].isPinnedContact;
      dispatch(toggleSnackbar(isPinnedContact ? 'Contact Pinned' : 'Contact Unpinned'));
    }
    return Promise.resolve(data);
  } catch (error) {
    dispatch(toggleSnackbar('Contact Pinning Failed!'));
    return Promise.resolve(error);
  }
};

export const inviteContacts = payload => async dispatch => {
  try {
    const requestBody = {
      contacts: payload,
    };

    const response = await postFetchAPI(API_ROUTES.inviteContacts, requestBody);
    if (response.error) throw response;
    const data = response.result;

    if (data && data.list && data.list.length) {
      const userName = payload && payload[0] && payload[0].name;
      dispatch(toggleSnackbar(`Invited ${userName}`));
    }
    return Promise.resolve(data);
  } catch (error) {
    dispatch(toggleSnackbar('Contact Invite Failed!'));
    return Promise.resolve(error);
  }
};

export const getContactsFromDevice = payload => async (dispatch, getStore) => {
  try {
    dispatch(toggleContactFetchFromDevice(true));
    const state = getStore();
    const userInfo = getUserInfo(state);
    const userLoggedIn = userInfo && userInfo.userId;
    Contacts.getAll().then(async contacts => {
      let mappedContacts = [];
      let i = 0;
      let len = contacts.length;
      if (len) {
        while (i < len) {
          let contact = contacts[i];
          let contactName = Platform.OS === 'ios' ? `${contact.givenName} ${contact.familyName}` : contact.displayName;
          let userEmail = contact.emailAddresses[0] && contact.emailAddresses[0].email;
          let mappedContactDetails = {name: contactName, email: validateEmail(userEmail) ? userEmail : null};
          let hasPhoneNumbers = contact.phoneNumbers && contact.phoneNumbers.length;
          if (hasPhoneNumbers) {
            let phoneNumberDictionery = {};
            contact.phoneNumbers.forEach(item => {
              if (item && item.number) {
                let phoneNumber = item.number.replace(/[()-]/g, '');
                let pn = PhoneNumber(phoneNumber);
                if (pn.isValid() && pn.isMobile()) {
                  let significantNumber = pn.getNumber('significant') || phoneNumber;
                  significantNumber = significantNumber.split(' ').join('');
                  let countryCode = '+' + pn.getCountryCode();
                  phoneNumberDictionery[significantNumber] = {phoneNumber: significantNumber, countryCode};
                }
              }
            });
            if (Object.values(phoneNumberDictionery).length) {
              Object.values(phoneNumberDictionery).forEach(item => {
                mappedContacts.push({...mappedContactDetails, ...item});
              });
            }
          }
          i++;
        }
      }

      const hasContactsInDevice = mappedContacts.length;
      let totalMappedContacts = [];

      if (hasContactsInDevice) {
        await ContactsORM.dropTable();
        verifyContacts(mappedContacts, async res => {
          const moozumoUsers = res && res.data && res.data.moozumoUsers;
          const nonMoozumoUsers = res && res.data && res.data.nonMoozumoUsers;
          const contactImportFinished = res.end > res.totalContacts;
          if (moozumoUsers.length) await dispatch(addContactToCloud(moozumoUsers));
          if (nonMoozumoUsers.length) await insertContacts(nonMoozumoUsers).then(response => {});
          if (contactImportFinished) {
            dispatch(getTotalAndMoozumoCount());
            dispatch(updateAfterContactsFetchedFromPhoneBook());
          }
        });
      } else {
        dispatch(toggleContactFetchFromDevice(false));
        dispatch(toggleSnackbar('Your Device Contacts is empty,please wait while we fetch contacts from cloud!'));
      }
    });
  } catch (error) {
    dispatch(toggleContactFetchFromDevice(false));
    dispatch(toggleSnackbar(error && error.message));
    return Promise.resolve(error);
  }
};

export const updateAfterContactsFetchedFromPhoneBook = () => async (dispatch, getState) => {
  const state = getState();
  const prevUserContacts = getUserContactsFromState(state);
  const limitBy = Math.ceil(prevUserContacts.length / 50);
  let pagination = {
    page: 1,
    limit: (limitBy ? limitBy : 1) * 50,
  };
  const userContacts = await loadContacts(pagination);
  await dispatch(setUserContacts(userContacts));
  dispatch(toggleContactFetchFromDevice(false));
  dispatch(toggleSnackbar('Your contact list has been updated'));
};

export const getContactsFromLocalDatabase = pagination => async (dispatch, getState) => {
  // getMoozumoUserCount();
  const userContacts = await loadContacts(pagination);
  return await dispatch(upsertContactsToStore(userContacts));
};

export const searchContacts = (searchKey, pagination) => async dispatch => {
  let results = [];
  if (searchKey) results = await searchUser(searchKey);
  else results = await loadContacts(pagination);
  dispatch(setUserContacts(results));
};

export const checkPermissionToReadContact = () => async dispatch => {
  check(Platform.OS === 'ios' ? PERMISSIONS.IOS.CONTACTS : PERMISSIONS.ANDROID.READ_CONTACTS)
    .then(result => dispatch(handleContactPermissionResult(result)))
    .catch(error => {});
};

export const getTotalAndMoozumoCount = () => async dispatch => {
  let data = {
    total: 0,
    moozumoUser: 0,
  };
  data.moozumoUser = await getMoozumoUserCount();
  data.total = await getTotalContactCount();

  dispatch({type: types.UPDATE_CONTACTS_COUNT, data});
};

export const requsetPermissionToReadContact = () => async dispatch => {
  request(Platform.OS === 'ios' ? PERMISSIONS.IOS.CONTACTS : PERMISSIONS.ANDROID.READ_CONTACTS)
    .then(result => dispatch(handleContactPermissionResult(result)))
    .catch(err => {});
};

export const getLatestContactsFromDevice = () => dispatch => {
  dispatch(checkPermissionToReadContact());
};

export const handleContactPermissionResult = result => dispatch => {
  switch (result) {
    case RESULTS.UNAVAILABLE:
      break;
    case RESULTS.DENIED:
      dispatch(requsetPermissionToReadContact());
      break;
    case RESULTS.LIMITED:
      dispatch(getContactsFromDevice());
      break;
    case RESULTS.GRANTED:
      dispatch(getContactsFromDevice());
      break;
    case RESULTS.BLOCKED:
      break;
  }
};

export const setUserContacts = data => {
  return {
    type: types.SET_USER_CONTACTS,
    data,
  };
};

export const setDeviceContacts = data => {
  return {
    type: types.SET_DEVICE_CONTACTS,
    data,
  };
};

export const toggleContactFetchFromDevice = data => {
  return {
    type: types.TOGGLE_CONTACT_FETCH_FROM_DEVICE,
    data,
  };
};
