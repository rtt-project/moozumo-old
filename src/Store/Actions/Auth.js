import * as types from '../actionTypes';
import {postFetchAPI, API_ROUTES, getFetchAPI} from '../../Helper/fetchAPI';
import {storeData, ASYNC_STORAGE_KEYS, removeFewData} from '../../Helper/asyncStorage';
import {toggleSnackbar} from './Global';

export const createUser = payload => async dispatch => {
  try {
    const response = await postFetchAPI(API_ROUTES.createUser, payload);

    if (response.error) throw response.error;
    const data = response.result;
    dispatch(toggleSnackbar('OTP Generated'));
    return Promise.resolve({...payload, ...data});
  } catch (error) {
    dispatch(toggleSnackbar(error && error.message));
    return Promise.resolve(error);
  }
};

export const verifyOTP = payload => async dispatch => {
  try {
    const response = await postFetchAPI(API_ROUTES.verifyOTP, payload);
    if (response.error) throw response.error;
    const data = response.result;
    await storeData(ASYNC_STORAGE_KEYS.authToken, data.authorizationToken);
    dispatch(setUserInfo({...payload, ...data}));
    return Promise.resolve(true);
  } catch (error) {
    dispatch(toggleSnackbar((error && error.message) || 'OTP Verification Failed'));
    return Promise.resolve(error);
  }
};

export const getUserInfo = userId => async dispatch => {
  try {
    const response = await getFetchAPI(API_ROUTES.getUserInfo);
    if (response.error) throw response.error;
    let data = response.result;
    data.userId = data.userid || data.userId;
    dispatch(setUserInfo(data));
    return Promise.resolve(true);
  } catch (error) {
    dispatch(logOutUser());
    dispatch(toggleSnackbar(error && error.message));
    return Promise.resolve(error);
  }
};

export const logOutUser = () => async dispatch => {
  try {
    await removeFewData(Object.values(ASYNC_STORAGE_KEYS));
    await dispatch(setUserInfo({}));
    return Promise.resolve(true);
  } catch (error) {
    // dispatch(toggleSnackbar('Your Session Expired!'));
    return Promise.resolve(error);
  }
};

export const setUserInfo = data => {
  return {
    type: types.SET_USER_INFO,
    data,
  };
};
