import * as types from '../actionTypes';
import Snackbar from 'react-native-snackbar';
export const toggleModal = (isVisible, component) => dispatch => {
  dispatch({
    type: types.TOGGLE_MODAL,
    payload: {
      isVisible,
      component,
    },
  });
};

export const toggleSnackbar = (message, variant) => () => {
  console.log('message: ', message);
  Snackbar.show({
    text: message ? JSON.stringify(message) : 'Something Went Wrong!',
    duration: Snackbar.LENGTH_SHORT,
  });
  return {
    type: 'NILL',
  };
};

export const setCallLog = data => dispatch => {
  return {
    type: types.SET_CALL_LOGS,
    data,
  };
};
