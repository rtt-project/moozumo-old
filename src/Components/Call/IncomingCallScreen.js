import React, {useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Image, Dimensions, Alert, ImageBackground, StatusBar} from 'react-native';
import {colors} from '../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../Constants/typography';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

const {width, height} = Dimensions.get('window');

const IncomingCallScreen = ({data = {}, onDecline, onAccept}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  return (
    <ImageBackground source={require('../../Assets/Images/mainbackground.png')} style={styles.ImageBackground}>
      <View style={{flex: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <LinearGradient colors={[colors.overlayGradientClr1, colors.overlayGradientClr2]}>
          <View style={styles.imageBar}>
            <View
              style={{
                backgroundColor: colors.devidergrey,
                height: 150,
                width: 150,
                borderRadius: 75,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: colors.black,
                borderColor: colors.white,
                borderWidth: 3,
                shadowOffset: {
                  width: 5,
                  height: 5,
                },
                shadowOpacity: 0.2,
                shadowRadius: 45,
                elevation: 8,
                marginTop: 20,
              }}>
              <Text style={{color: colors.black, ...typographyVariant.the60, ...typographyWeight.bold, textAlign: 'center'}}>{(data && data.name && data.name[0]) || (data && data.userName && data.userName[0])}</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Text style={{...typographyVariant.the45, color: colors.white, marginTop: 10, textAlign: 'center'}}>{data.name || data.userName}</Text>
              <Text style={{color: colors.white, ...typographyVariant.the15, textAlign: 'center'}}>{`Mobile ${data.countryCode} ${data.phoneNumber}`}</Text>
              <Text style={{color: colors.white, ...typographyVariant.the19, marginTop: 10, textAlign: 'center'}}>Moozumo text call</Text>
            </View>
          </View>
        </LinearGradient>
        <View style={styles.topBar}>
          <Modal isVisible={isModalVisible} backdropColor={colors.blackOverlay40} onBackdropPress={() => toggleModal()}>
            <View style={{backgroundColor: colors.devidergrey, borderRadius: 10.0}}>
              <View style={{margin: 20.0}}>
                <Text style={{color: colors.black, ...typographyVariant.the18}}>I'll call you back</Text>
                <View style={{height: 1, backgroundColor: colors.seperator, marginVertical: 5}}></View>
                <Text style={{color: colors.black, ...typographyVariant.the18}}>Can't Attend now, call me later</Text>
                <View style={{height: 1, backgroundColor: colors.seperator, marginVertical: 5}}></View>
                <Text style={{color: colors.black, ...typographyVariant.the18}}>I am busy now, call you later</Text>
                <View style={{height: 1, backgroundColor: colors.seperator, marginVertical: 5}}></View>
                <Text style={{color: colors.black, ...typographyVariant.the18}}>Custom message</Text>
              </View>
            </View>
          </Modal>
        </View>

        <View style={styles.bottomBar}>
          <TouchableOpacity style={{alignItems: 'center'}} onPress={() => onDecline(data)}>
            <Image style={styles.iconImg} source={require('../../Assets/Images/decline_icon.png')} />
            <Text style={{color: colors.white, ...typographyVariant.the14}}>Decline</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{alignItems: 'center'}} onPress={() => onAccept(data)}>
            <Image style={styles.iconImg} source={require('../../Assets/Images/accept_btn.png')} />
            <Text style={{color: colors.white, ...typographyVariant.the14}}>Accept</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{alignItems: 'center'}} onPress={() => toggleModal()}>
            <Image style={styles.iconImg} source={require('../../Assets/Images/message_btn.png')} />
            <Text style={{color: colors.white, ...typographyVariant.the14}}>Message</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  imageBar: {
    height: height * 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: colors.blackOverlay
  },
  topBar: {
    height: height * 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
  },
  image: {
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 200 / 2,
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: 'black',
  },
  imagegrp: {
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 150 / 2,
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: 'black',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#e20e30',
    marginTop: 250,
  },

  title: {
    color: '#fff',
    ...typographyVariant.the45,
    ...typographyWeight.bold,
  },
  titlegrp: {
    color: '#000000',
    fontSize: 40,
    ...typographyWeight.bold,
  },
  subText: {
    color: '#fff',
    ...typographyVariant.the15,
  },
  iconImg: {
    height: 70,
    width: 70,
    alignSelf: 'center',
  },
  iconImgGreen: {
    height: 32,
    width: 32,
    alignSelf: 'center',
  },
  iconImgGrey: {
    height: 20,
    width: 20,
    alignSelf: 'center',
  },
  btnStopCall: {
    height: 65,
    width: 65,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 32,
    backgroundColor: '#FF0000',
    position: 'absolute',
    bottom: 160,
    left: '40%',
    zIndex: 1,
  },
  btnAction: {
    height: 60,
    width: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60 / 2,
    backgroundColor: '#ff400a',
  },
  btnActionGreen: {
    height: 60,
    width: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60 / 2,
    backgroundColor: '#36b549',
  },
  btnActionGrey: {
    height: 40,
    width: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40 / 2,
    backgroundColor: '#453939',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  ImageBackground: {
    flex: 1,
    width: null,
    height: null,
  },
});

export default IncomingCallScreen;
