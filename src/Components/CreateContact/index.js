import React from 'react';
import {View, Text, KeyboardAvoidingView, Image, ScrollView, TouchableOpacity, Dimensions, ActivityIndicator} from 'react-native';
import {useDispatch} from 'react-redux';
import CustomTextInput from '../Common/CustomInput';
import MaterialInput from '../Common/MaterialInput';
import {useForm} from 'react-hook-form';
import CustomButton from '../Common/CustomButton';
import {addContact, storeContactToDevice} from '../../Store/Actions/Contacts';
import Svg, {Circle, Ellipse, Rect, Defs, LinearGradient, Stop} from 'react-native-svg';
import {colors} from '../../Constants/colors';
import {typography, typographyVariant, typographyWeight} from '../../Constants/typography';
import PhoneInput from '../Common/PhoneInput';
import {toggleModal} from '../../Store/Actions/Global';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import PhoneNumber from 'awesome-phonenumber';
import cleanPhoneNumber from '../../Helper/cleanPhoneNumber';
import validateEmail from '../../Helper/validateEmail';
const {width, height} = Dimensions.get('window');
const CreateContact = ({navigation}) => {
  const [isLoading, setIsLoading] = React.useState(false);

  const {
    getValues,
    control,
    trigger,
    watch,
    register,
    setValue,
    formState: {errors, isValid, isValidating, isSubmitting},
  } = useForm({
    mode: 'onSubmit',
    reValidateMode: 'onChange',
    defaultValues: {
      name: '',
      phoneNumber: '',
      countryCode: '+1',
      email: '',
    },
    shouldFocusError: true,
  });

  const dispatch = useDispatch();

  const handleOnSubmit = async () => {
    const isValidated = await trigger();
    if (isValidated) {
      setIsLoading(true);
      let data = getValues();
      data.phoneNumber = cleanPhoneNumber(data.phoneNumber);
      await dispatch(storeContactToDevice(data));
      navigation.goBack();
      setIsLoading(false);
    }
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => popOver()}>
          <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15}}>
            <Image style={{width: 4.5, height: 18}} source={require('../../Assets/Images/threedots.png')} />
          </View>
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={{flex: 1, backgroundColor: colors.white}}>
      <ScrollView keyboardShouldPersistTaps="always" style={{flex: 1}}>
        <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginVertical: 20}}>
          <View style={{width: 130, height: 130, backgroundColor: colors.primary, borderRadius: 130 / 2, justifyContent: 'center', alignItems: 'center'}}>
            <Image style={{width: 50, height: 50, position: 'absolute', alignSelf: 'center', tintColor: colors.white}} source={require('../../Assets/Images/default_avatar.png')} />
          </View>
        </View>
        <View style={{flex: 1, padding: 10, backgroundColor: '#fff', padding: 20}}>
          <View style={{paddingVertical: 5}}>
            <MaterialInput editable={!isLoading} required label="Name*" name="name" control={control} error={errors && errors.name}></MaterialInput>
          </View>
          <View style={{paddingVertical: 5}}>
            <PhoneInput watch={watch} register={register} control={control} errors={errors} setValue={setValue} />
          </View>
          <View style={{paddingVertical: 5}}>
            <MaterialInput
              editable={!isLoading}
              required={false}
              controllerRules={{
                validate: value => {
                  if (value) return validateEmail(value);
                  else return true;
                },
              }}
              label="Email (Optional)"
              name="email"
              control={control}
              errorMessage={errors && errors.email && errors.email.type === 'validate' ? 'Invalid Email' : ''}
              error={errors && errors.email}></MaterialInput>
          </View>
          <View style={{paddingTop: 30, flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              disabled={isLoading}
              onPress={() => handleOnSubmit()}
              style={{
                padding: 10,
                backgroundColor: colors.primary,
                alignItems: 'center',
                width: width * 0.4,
              }}>
              <View style={{flexDirection: 'row'}}>
                {isLoading ? (
                  <ActivityIndicator color="#fff" />
                ) : (
                  <>
                    <Text style={{...typographyVariant.the12, ...typographyWeight.bold, color: colors.white}}>Add to contact</Text>
                    <Image style={{marginLeft: 16, width: 16, height: 16, alignSelf: 'flex-end', tintColor: colors.white}} source={require('../../Assets/Images/buttonarrow.png')} />
                  </>
                )}
              </View>
            </TouchableOpacity>
            <View style={{paddingHorizontal: 20}}>
              <TouchableOpacity disabled={isLoading} onPress={() => navigation.goBack()}>
                <Text style={{...typographyVariant.the12, ...typographyWeight.bold, color: colors.seconderytextgrey}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default CreateContact;
