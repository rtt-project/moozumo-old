import React, { useState } from 'react';
import { SafeAreaView, ActivityIndicator, Animated, View, Text, TouchableOpacity, TouchableHighlight, Image, StyleSheet, Dimensions, StatusBar, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Svg, { Circle } from 'react-native-svg';
import { colors } from '../../Constants/colors';
import { typography, typographyVariant, typographyWeight } from '../../Constants/typography';
import { CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell } from 'react-native-confirmation-code-field';
import { useDispatch } from 'react-redux';

import styles, { ACTIVE_CELL_BG_COLOR, CELL_BORDER_RADIUS, CELL_SIZE, DEFAULT_CELL_BG_COLOR, NOT_EMPTY_CELL_BG_COLOR } from './Styles';
import { verifyOTP, createUser } from '../../Store/Actions/Auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const { Value, Text: AnimatedText } = Animated;
const CELL_COUNT = 4;

const { width, height } = Dimensions.get('window');

const NumberKeyboard = [{ label: 1, value: 1 }, { label: 2, value: 2 }, { label: 3, value: 3 }, { label: 4, value: 4 }, { label: 5, value: 5 }, { label: 6, value: 6 }, { label: 7, value: 7 }, { label: 8, value: 8 }, { label: 9, value: 9 }, { isEmpty: true }, { label: 0, value: 0 }, { isClear: true }];

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));

const animateCell = ({ hasValue, index, isFocused }) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const VerifyOtp = ({ route, navigation }) => {
  const dispatch = useDispatch();
  const params = route && route.params;
  const [isLoading, setIsLoading] = React.useState(false);
  const [isBLoading, setIsBLoading] = React.useState(false);
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const [userDetails, setUserDetails] = React.useState(params.userDetails);

  const renderCell = ({ index, symbol, isFocused }) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
          inputRange: [0, 1],
          outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
        })
        : animationsColor[index].interpolate({
          inputRange: [0, 1],
          outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
        }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [CELL_SIZE, CELL_BORDER_RADIUS],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.9, 1],
          }),
        },
      ],
    };

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    setTimeout(() => {
      animateCell({ hasValue, index, isFocused });
    }, 0);

    return (
      <AnimatedText key={index} style={[styles.cell, animatedCellStyle]} onLayout={getCellOnLayoutHandler(index)}>
        {symbol || (
          <Cursor
            // Blinking animation speed (optional, number)
            delay={10000}
            // Symbol that would be returned to simulate cursor blinking (optional, string)
            cursorSymbol="-"
          />
        )}
      </AnimatedText>
    );
  };
  const phoneNumberMask = phoneNumberToMask => {
    const phoneNumberToMaskCopy = phoneNumberToMask;
    const phoneNumberLength = phoneNumberToMaskCopy.length;
    const phoneNumberShortLength = phoneNumberToMaskCopy.length - 3;
    const subPhoneNumber = phoneNumberToMaskCopy.substr(1, phoneNumberShortLength);
    const subPhoneNumberLength = subPhoneNumber.length;
    var subPhonenumberStars = '';
    for (let i = 0; i < subPhoneNumberLength; i++) {
      subPhonenumberStars = subPhonenumberStars + '*';
    }
    const maskedPhoneNumber = phoneNumberToMaskCopy[0] + subPhonenumberStars + phoneNumberToMaskCopy[phoneNumberLength - 2] + phoneNumberToMaskCopy[phoneNumberLength - 1];
    return maskedPhoneNumber;
  };
  const handleSendOtp = async () => {
    setIsBLoading(true);
    if (value.length) {
      await dispatch(verifyOTP({ ...userDetails, otp: value }));
    }
    setIsBLoading(false);
  };

  const handleResendOtp = async () => {
    setIsLoading(true);
    let resendOtp = await dispatch(createUser(userDetails));
    setUserDetails(resendOtp);
    setIsLoading(false);
  };

  return (
    <LinearGradient colors={[colors.secondery, colors.primary]} style={{ flex: 1, height: height }}>
      <StatusBar translucent backgroundColor="transparent" />
      <Svg style={{ position: 'absolute' }} height="100%" width="100%" viewBox="0 50 100 100">
        <Circle cx="50" cy="0" r={height * 0.11} fill={colors.seconderyblue} />
      </Svg>
      <Text
        style={{
          marginTop: height * 0.1,
          marginBottom: height * 0.04,
          alignSelf: 'center',
          ...typographyVariant.the22,
        }}>
        Verification Code
      </Text>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingTop: height * 0.03,
            alignItems: 'center',
          }}>
          <Image style={{ width: 50, height: 33 }} source={require('../../Assets/Images/Phone_icon.png')} />
          <View style={{ marginLeft: 16 }}>
            <Text style={{ color: colors.white, ...typographyVariant.the18 }}>Please type the verification (OTP)</Text>
            {userDetails && userDetails.phoneNumber ? <Text style={{ color: colors.white, ...typographyVariant.the18 }}>Code sent to {userDetails.countryCode + ' ' + phoneNumberMask(userDetails.phoneNumber)}</Text> : null}
            {/* <Text style={{color: colors.white, ...typographyVariant.the18}}>Code sent to {userDetails.countryCode + ' ' + phoneNumberMask(userDetails.phoneNumber)}</Text> */}
          </View>
        </View>
        <CodeField
          ref={ref}
          // showSoftInputOnFocus={false}
          {...props}
          value={value}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType="number-pad"
          textContentType="oneTimeCode"
          renderCell={renderCell}
          editable={false}
        />
        <TouchableOpacity
          disabled={isLoading}
          onPress={() => handleResendOtp()}
          style={{
            marginLeft: 12,
            paddingTop: height * 0.01,
            paddingLeft: 20,
            marginRight: 20,
            alignItems: 'flex-start',
          }}>
          {isLoading ? (<ActivityIndicator color="#fff" />) : (
            <Text style={{ color: colors.white, ...typographyVariant.the19 }}>RESEND CODE {'>'}</Text>
          )}
        </TouchableOpacity>
        <View style={{ marginLeft: 30, marginTop: height * 0.01 }}>
          <TouchableOpacity
            disabled={isBLoading || (value.length < 4)}
            onPress={() => handleSendOtp()}
            style={{
              padding: 10,
              backgroundColor: colors.white,
              alignItems: 'center',
              // minWidth: width * 0.4,
              maxWidth: width / 2,
            }}>
            {isBLoading ? (<ActivityIndicator color={colors.primary} style={{ width: width * 0.33 }} />) : (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ ...typographyVariant.the17, color: colors.primary, ...typographyWeight.bold }}>Verify and Proceed</Text>
              <Image
                style={{
                  marginLeft: 16,
                  width: 16,
                  height: 16,
                  alignSelf: 'flex-end',
                }}
                source={require('../../Assets/Images/buttonarrow.png')}
              />
            </View>)}
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            flexWrap: 'wrap',
            paddingTop: height * 0.01,
          }}>
          {NumberKeyboard.map((item, index) => {
            return (
              <View
                key={index}
                style={{
                  minWidth: width / 3 - 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {!item.isEmpty && !item.isClear && (
                  <TouchableHighlight
                    underlayColor={Colors.white}
                    onPress={() => {
                      if (value.length < 4) setValue(value + String(item.value));
                    }}
                    style={{
                      width: width / 3,
                      height: height / 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        ...typographyVariant.the45,
                        color: colors.white,
                      }}>
                      {item.label}
                    </Text>
                  </TouchableHighlight>
                )}
                {item.isClear && (
                  <TouchableHighlight
                    underlayColor={Colors.white}
                    onPress={() => (value.length > 0 ? setValue(value.slice(0, value.length - 1)) : {})}
                    style={{
                      width: width / 3 - 60,
                      height: height / 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image style={{ width: 50, height: 33, alignSelf: 'flex-end' }} source={require('../../Assets/Images/backspace_arrow.png')} />
                  </TouchableHighlight>
                )}
              </View>
            );
          })}
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default VerifyOtp;
