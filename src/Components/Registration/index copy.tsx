import React from 'react';
import {View, Text, TouchableOpacity, FlatList, Image, StyleSheet, Dimensions, StatusBar, KeyboardAvoidingView, ScrollView, Platform, ActivityIndicator, SafeAreaView} from 'react-native';
import Svg, {Circle, Ellipse, Rect, Defs, LinearGradient, Stop} from 'react-native-svg';
import {colors} from '../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../Constants/typography';
import MaterialInput from '../Common/MaterialInput';
import PhoneInput from '../Common/PhoneInput';
import {useForm, Controller} from 'react-hook-form';
import VersionInfo from 'react-native-version-info';
import {useDispatch} from 'react-redux';
import {createUser} from '../../Store/Actions/Auth';
import cleanPhoneNumber from '../../Helper/cleanPhoneNumber';

const {width, height} = Dimensions.get('window');
const Registration = ({navigation}: {navigation: {push: Function}}) => {
  const [isLoading, setIsLoading] = React.useState(false);

  const dispatch = useDispatch();

  const {
    getValues,
    control,
    trigger,
    watch,
    setValue,
    register,
    formState: {errors, isValid, isValidating},
  } = useForm({
    mode: 'onSubmit',
    reValidateMode: 'onChange',
    defaultValues: {
      name: '',
      phoneNumber: '',
      countryCode: '+1',
    },
    shouldFocusError: true,
  });

  const handleValidation = async () => {
    try {
      let isValidated = await trigger();
      if (isValidated) handleRegisterUser();
    } catch (err) {
      // alert('Error On Submit');
    }
  };

  const handleRegisterUser = async () => {
    setIsLoading(true);
    let data = getValues();
    data.phoneNumber = cleanPhoneNumber(data.phoneNumber);
    const response = await dispatch(createUser(data));
    navigation.push('VerifyOtp', {userDetails: {...data, ...response}});
    setIsLoading(false);
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar translucent backgroundColor="transparent" />
      <Svg style={{position: 'absolute'}} height="444" width="100%" viewBox="0 0 100 100">
        <Defs>
          <LinearGradient id="grad" x1="0" y1="0" x2="1" y2="1">
            <Stop offset="0" stopColor={colors.secondery} stopOpacity="1" />
            <Stop offset="1" stopColor={colors.primary} stopOpacity="1" />
          </LinearGradient>
        </Defs>
        <Ellipse cx="50" cy="0" rx="70" ry="120" fill="url(#grad)" />
      </Svg>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Image
          style={{
            // marginTop: 100,
            // marginBottom: 60,
            width: width * 0.4,
            height: width * 0.5,
            alignSelf: 'center',
          }}
          source={require('../../Assets/Images/logo_login.png')}
        />
      </View>

      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={{flex: 1.5}}>
        <ScrollView style={{flex: 1}}>
          <View
            style={{
              marginLeft: 20,
              padding: 20,
              marginRight: 20,
              marginBottom: 10,

              backgroundColor: '#fff',
              shadowColor: '#00000080',
              shadowOffset: {
                width: 0,
                height: 0,
              },
              shadowOpacity: 0.8,
              shadowRadius: 2,

              elevation: 4,
            }}>
            <Text
              style={{
                color: colors.black,
                ...typographyVariant.the25,
                ...typographyWeight.bold,
              }}>
              Create Account
            </Text>
            <Text style={{color: colors.seconderytextgrey, ...typographyVariant.the16}}>Enter name and mobile number to create an account.</Text>
            <View>
              <MaterialInput editable={!isLoading} required label="Name" name="name" control={control} error={errors && errors.name}></MaterialInput>
              <View style={{marginTop: 15}}>{/* <PhoneInput useRef="phone" /> */}</View>

              <PhoneInput watch={watch} register={register} control={control} errors={errors} setValue={setValue} />
            </View>

            <Text style={{color: colors.textgrey, fontSize: 15}}>We will send you a one-time password (OTP).</Text>
            <Text style={{color: colors.textgrey, fontSize: 13}}>Carrier rates may apply.</Text>
            <TouchableOpacity
              disabled={isLoading}
              onPress={() => handleValidation()}
              style={{
                padding: 10,
                marginTop: 30,
                backgroundColor: colors.primary,
                alignItems: 'center',
                width: width * 0.4,
              }}>
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: width * 0.33}}>
                {isLoading ? (
                  <ActivityIndicator color="#fff" />
                ) : (
                  <>
                    <Text
                      style={{
                        ...typographyVariant.the14,
                        ...typographyWeight.bold,
                        color: colors.white,
                      }}>
                      Send OTP
                    </Text>

                    <Image
                      style={{
                        marginLeft: 16,
                        width: 16,
                        height: 16,
                        alignSelf: 'flex-end',
                        tintColor: colors.white,
                      }}
                      source={require('../../Assets/Images/buttonarrow.png')}
                    />
                  </>
                )}
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <SafeAreaView style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{...typographyVariant.the13}}>{`Version ${VersionInfo.appVersion} - Prototype`}</Text>
      </SafeAreaView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    margin: 10,
  },
  logo: {
    width: 66,
    height: 58,
  },
});
export default Registration;
