import React from 'react';
import {View, Text, FlatList, Dimensions} from 'react-native';
import {colors} from '../../../Constants/colors';
import Stepper from '../../Common/Stepper';
import StepConnector from '../../Common/Stepper/StepConnector';
import StepContent from '../../Common/Stepper/StepContent';
import {typographyVariant, typographyWeight} from '../../../Constants/typography';
const {width, height} = Dimensions.get('window');
const UserActivities = ({name}) => {
  return (
    <View style={{flex: 1, marginHorizontal: 30, marginVertical: 10}}>
      <FlatList
        contentContainerStyle={{paddingBottom: 100}}
        data={Array(10).fill(0)}
        keyExtractor={(_, index) => index.toString()}
        renderItem={() => (
          <Stepper>
            <StepConnector>
              <View style={{width: 50, height: 50, borderRadius: 20, backgroundColor: colors.violet, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{color: colors.white, ...typographyVariant.the14}}>02</Text>
                <Text style={{color: colors.white, ...typographyVariant.the14}}>Sat</Text>
              </View>
            </StepConnector>
            <StepContent>
              <Text style={{...typographyVariant.the12, color: colors.textgrey}}>10.30 PM</Text>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <Text numberOfLines={1} style={{...typographyVariant.the14, ...typographyWeight.bold, maxWidth: width * 0.4}}>{`${name}'s`}</Text>
                  <Text style={{...typographyVariant.the14, marginLeft: 6}}>text call saved</Text>
                </View>
                <Text style={{color: colors.violettext, ...typographyVariant.the12, ...typographyWeight.bold}}>View saved call &gt;</Text>
              </View>
              <View style={{backgroundColor: colors.devidergrey, height: 1, marginVertical: 10}} />
              <Text style={{...typographyVariant.the12, color: colors.textgrey}}>12.30 PM</Text>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <Text numberOfLines={1} style={{...typographyVariant.the14, ...typographyWeight.bold, maxWidth: width * 0.4}}>{`${name}'s`}</Text>
                  <Text style={{...typographyVariant.the14, marginLeft: 6}}>text call saved</Text>
                </View>
                <Text style={{color: colors.violettext, ...typographyVariant.the12, ...typographyWeight.bold}}>View saved call &gt;</Text>
              </View>
              <View style={{backgroundColor: colors.devidergrey, height: 1, marginVertical: 10}} />
            </StepContent>
          </Stepper>
        )}
      />
    </View>
  );
};

export default UserActivities;
