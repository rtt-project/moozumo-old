import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Dimensions, ImageBackground } from 'react-native';
import { usePubNub } from 'pubnub-react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../Constants/colors';
import { typographyVariant, typographyWeight } from '../../Constants/typography';
import UserActivities from './UserActivities';
import { getUserInfo } from '../../Store/reduxSelectors';
import useCallActions from '../../Helper/useCallActions';
import { senderInfoType, messagePayload } from '../../Constants/types';
import messageEvents from '../../Constants/messageEvents';
import { pinContacts } from '../../Store/Actions/Contacts';
const { width, height } = Dimensions.get('window');

const CALL_ACTION_TYPES = {
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_REJECTED: 'CALL_REJECTED',
};

const ContactDetails = ({ route, navigation }) => {
  const pubnub = usePubNub();
  const dispatch = useDispatch();
  const [callAction] = useCallActions();

  const params = route && route.params;
  const { contactDetails } = params;

  const userInfo = useSelector(state => getUserInfo(state));
  const [isPinnedContact, setIsPinnedContact] = React.useState(contactDetails.isPinnedContact);

  const handleFavorite = async () => {
    let data = contactDetails;
    data.isPinnedContact = !isPinnedContact;
    let pinContactsResult = await dispatch(pinContacts([data]));
    if (pinContactsResult.status && (pinContactsResult.status == "NOT_SUCCESS")) {
      data.isPinnedContact = !(!isPinnedContact);
      console.log("Message fjdj ");
      console.log(pinContactsResult.status);
    }
    else {
      setIsPinnedContact(!isPinnedContact);
    }
  };

  const handleMakeACall = senderInfo => {
    let payload = {
      channelId: senderInfo.userId,
      event: messageEvents.incomingCall,
      to: senderInfo.userId,
      from: userInfo.userId,
      metadata: {
        senderInfo: userInfo,
      },
    };
    callAction.callThisUser(payload, senderInfo);
  };

  const noActivities = true;

  return (
    <View style={{ flex: 1 }}>
      <View style={{ alignItems: 'center', paddingVertical: 10, backgroundColor: colors.contacttopbggrey }}>
        <ImageBackground style={{ width: 155, height: 155, alignItems: 'center', justifyContent: 'center' }} source={require('../../Assets/Images/profileimage_bg.png')}>
          <View
            style={[
              styles.tinyLogo,
              {
                borderColor: colors.white,
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}>
            <Text style={{ ...typographyVariant.the60, ...typographyWeight.bold }}>{contactDetails.name && contactDetails.name[0]}</Text>
          </View>
        </ImageBackground>

        <Text numberOfLines={1} style={{ color: colors.seconderytextgrey, ...typographyVariant.the30 }}>
          {contactDetails.name}
        </Text>
        <View style={{ flexDirection: 'row', width: width, justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => callAction.redirectToChatScreen(contactDetails, false)}
            style={{
              padding: 10,
              alignItems: 'center',
            }}>
            <Image style={{ width: 60, height: 47 }} source={require('../../Assets/Images/message_button.png')} />
            <Text style={{ ...typographyVariant.the12, color: colors.seconderytextgrey }}>Message</Text>
          </TouchableOpacity>
          <View style={{ width: 2, backgroundColor: colors.devidergrey, marginVertical: 12, marginHorizontal: 8 }} />
          <TouchableOpacity
            onPress={() => handleMakeACall(contactDetails)}
            style={{
              padding: 10,
              alignItems: 'center',
            }}>
            <Image style={{ width: 60, height: 47 }} source={require('../../Assets/Images/textcall_button.png')} />
            <Text style={{ ...typographyVariant.the12, color: colors.seconderytextgrey }}>Text Call</Text>
          </TouchableOpacity>
          <View style={{ width: 2, backgroundColor: colors.devidergrey, marginVertical: 12, marginHorizontal: 8 }} />
          <TouchableOpacity
            onPress={() => handleFavorite()}
            style={{
              padding: 10,
              alignItems: 'center',
            }}>
            <Image style={{ width: 60, height: 47 }} source={require('../../Assets/Images/pinned_button.png')} />
            <Text style={{ ...typographyVariant.the12, color: colors.seconderytextgrey }}>{isPinnedContact ? 'Unpin Contact' : 'Pin Contact'}</Text>
          </TouchableOpacity>
        </View>
      </View>

      {noActivities && (
        <View style={{ backgroundColor: colors.greybg, height: height * 0.6, alignItems: 'center', justifyContent: 'center' }}>
          <Image style={{ width: 150, height: 145 }} source={require('../../Assets/Images/noactivity_icon.png')} />
          <Text style={{ ...typographyVariant.the15, color: colors.textgrey, marginTop: 5 }}>No activities available</Text>
        </View>
      )}
      {!noActivities && (
        <View style={{ backgroundColor: colors.greybg, height: height * 0.6 }}>
          <View style={{ marginHorizontal: 40, marginVertical: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ ...typographyVariant.the15PinchMore, color: colors.seconderytextgrey, marginTop: 5 }}>Activities of</Text>
              <Text numberOfLines={1} style={{ ...typographyVariant.the15PinchMore, ...typographyWeight.bold, color: colors.seconderytextgrey, marginTop: 5, marginLeft: 8, width: width * 0.5 }}>
                {contactDetails.name}
              </Text>
            </View>
          </View>
          <UserActivities name={contactDetails.name} />
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    margin: 10,
  },
  logo: {
    width: 66,
    height: 58,
  },
});
export default ContactDetails;
