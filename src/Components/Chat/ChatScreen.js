import React, {useState} from 'react';
import {Keyboard, View, Text, TouchableOpacity, Dimensions, NativeModules, LayoutAnimation, TextInput, ScrollView, Image, StyleSheet, KeyboardAvoidingView, Button, Platform, ActivityIndicator} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('screen');

const {UIManager} = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const ChatScreen = ({name, avatar, isGroupCall, isTextArea, messages, handleOnChange, openModal, callStatusByUserId}) => {
  const [keyboardHeight, setkeyboardHeight] = useState(0);
  const [normalHeight, setnormalHeight] = useState(0);
  const [shortHeight, setshortHeight] = useState(0);
  const [ht, setht] = useState(height / 2);
  const [messageHistory, setMessageHistory] = React.useState('');

  const [currentMessage, setCurrentMessage] = React.useState('');

  const scrollViewRef = React.useRef('');
  const currentMessageRef = React.useRef(null);
  currentMessageRef.current = currentMessage;
  React.useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  React.useEffect(() => {
    const shouldScrollToBottom = messages && scrollViewRef && scrollViewRef.current && scrollViewRef.current.scrollToEnd;
    if (shouldScrollToBottom) scrollViewRef.current.scrollToEnd({animated: true});
  }, [messages]);

  const _keyboardDidShow = e => {
    LayoutAnimation.easeInEaseOut();
    setkeyboardHeight(e.endCoordinates.height);
    setnormalHeight(Dimensions.get('window').height);
    setshortHeight(Dimensions.get('window').height - e.endCoordinates.height);
    setht((Dimensions.get('window').height - e.endCoordinates.height) / 2.33);
  };

  const _keyboardDidHide = e => {
    LayoutAnimation.easeInEaseOut();
    setkeyboardHeight(e.endCoordinates.height);
    setnormalHeight(Dimensions.get('window').height);
    setshortHeight(Dimensions.get('window').height - e.endCoordinates.height);
    setht(Dimensions.get('window').height / 2);
  };

  const handleOnKeyPress = event => {
    const keyPressedIsEnter = event.nativeEvent.key === 'Enter';

    if (keyPressedIsEnter) {
      setMessageHistory(messageHistory.trim() + '\n' + currentMessage.trim());
      setCurrentMessage('');
    }
  };
  console.log('messageHistory: ', messageHistory);
  return (
    <View
      style={{
        flex: 1,
        // borderWidth: StyleSheet.hairlineWidth,
        maxHeight: ht,
      }}>
      <LinearGradient colors={isTextArea ? ['#d1b4f5', '#fff'] : ['#9ce0fb', '#fff']}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            // backgroundColor: isTextArea ? '#2b0757' : '#13a5e0',
            padding: 2,
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            {isGroupCall ? (
              avatar.map((item, index) => (
                <View style={{position: 'relative'}} key={index}>
                  {callStatusByUserId.hasOwnProperty(item.id) ? (
                    callStatusByUserId[item.id] ? (
                      <Image
                        key={index}
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          marginRight: 5,
                        }}
                        source={{
                          uri: item.profilePic,
                        }}
                      />
                    ) : null
                  ) : (
                    <>
                      <Image
                        key={index}
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          marginRight: 5,
                          backgroundColor: '#eee',
                          opacity: 0.5,
                        }}
                        source={{
                          uri: item.profilePic,
                        }}
                      />
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          marginRight: 5,
                          position: 'absolute',
                          left: 0,
                          right: 0,
                          bottom: 0,
                          top: 0,
                        }}>
                        <ActivityIndicator color="#000000" />
                      </View>
                    </>
                  )}
                </View>
              ))
            ) : (
              // <View
              //   style={{
              //     width: 30,
              //     height: 30,
              //     borderRadius: 15,
              //     backgroundColor: '#fff',
              //     marginRight: 10,
              //   }}>

              // </View>
              <View
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  backgroundColor: '#fff',
                  marginRight: 10,
                }}>
                <Image
                  style={{width: 30, height: 30, borderRadius: 15}}
                  source={{
                    uri: avatar,
                  }}
                />
              </View>
            )}
            {name ? (
              <View>
                <Text
                  style={{
                    color: '#030207',
                    ...typographyWeight.bold,
                  }}>
                  {name}
                </Text>
              </View>
            ) : null}
          </View>
          {!isTextArea && isGroupCall ? (
            <View style={{alignContent: 'flex-end'}}>
              <TouchableOpacity onPress={() => openModal()}>
                <Text
                  style={{
                    color: '#030207',
                    ...typographyWeight.bold,
                    fontSize: 24,
                  }}>
                  +
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </LinearGradient>

      {!isTextArea ? (
        <ScrollView
          ref={scrollViewRef}
          bounces={false}
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            padding: 10,

            // paddingBottom: 50,
            // flex: 1,
          }}>
          {/* <Image style={{width: 30, borderRadius: 15, height: 30, borderRadius: 15, marginRight: 10}} source={{uri: item.profilePic}} />  */}
          {messages.map((item, index) => (
            <View key={index} style={{paddingVertical: 5}}>
              {item.name ? <Text style={{...typographyWeight.bold, lineHeight: 25}}>{item.name}</Text> : null}
              <Text key={index}>{item.message || item}</Text>
            </View>
          ))}
        </ScrollView>
      ) : null}

      {isTextArea && (
        <View
          style={{
            padding: 10,
            flex: 1,
          }}>
          <View style={{flex: 0.5}}>
            <ScrollView style={{flex: 1}}>
              <Text>{messageHistory}</Text>
              {/* <View style={{flex: 1}}>
              <TextInput
                // value={messages[messages.length]}
                value={messageHistory}
                multiline={true}
                editable={false}
                focusable={true}
              />
            </View> */}
            </ScrollView>
          </View>
          <TextInput
            // value={messages[messages.length]}
            style={{
              flex: 1,
              textAlignVertical: 'top',
              paddingBottom: 20,
              backgroundColor: 'grey',
            }}
            value={currentMessage}
            autoFocus={true}
            multiline={true}
            onChangeText={value => {
              console.log('value: ', value);
              // handleOnChange(value);
              setCurrentMessage(value);
            }}
            underlineColorAndroid="transparent"
            onSubmitEditing={({text}) => {
              // handleOnChange('nextLine');
            }}
            onKeyPress={handleOnKeyPress}
          />
        </View>
      )}
    </View>
  );
};

export default ChatScreen;
