import React, {useState} from 'react';
import {Keyboard, View, Text, TouchableOpacity, Dimensions, NativeModules, LayoutAnimation, TextInput, ScrollView, Image, StyleSheet, KeyboardAvoidingView, Button, Platform, ActivityIndicator, StatusBar} from 'react-native';
import {PubNubProvider, usePubNub} from 'pubnub-react';
import {useSelector} from 'react-redux';
import {getUserInfo} from '../../Store/reduxSelectors';
import {useNavigation} from '@react-navigation/native';

const {width, height} = Dimensions.get('screen');

const {UIManager} = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

// const ChatBoxSender = ({name, receiverInfo}) => {
const ChatBoxSender = ({name, receiverInfo}) => {
  const pubnub = usePubNub();
  const navigation = useNavigation();
  const userInfo = useSelector(state => getUserInfo(state));
  const [reaceiverMessages, setReaceiverMessages] = React.useState([]);

  const scrollViewRef = React.useRef('');

  const handleMessage = event => {
    const message = event.message;
    const payload = JSON.parse(message);
    const shouldHangUp = payload.event === 'callHangup';
    const itIsYourMessage = payload.from === receiverInfo.userId && payload.to === userInfo.userId;
    const showMessage = payload.event === 'message' && payload.from === receiverInfo.userId;
    if (itIsYourMessage) {
      if (shouldHangUp) navigation.goBack();
      if (showMessage) {
        const text = payload.message || payload.text;
        const isNextLine = text === 'nextLine';
        if (isNextLine) setReaceiverMessages(messages => [...messages]);
        else setReaceiverMessages([text]);
      }
    }
  };
  React.useEffect(() => {
    pubnub.addListener({message: handleMessage});
    return () => pubnub.removeListener();
  }, [pubnub]);

  React.useEffect(() => {
    return () => pubnub.removeListener();
  }, []);

  React.useEffect(() => {
    const shouldScrollToBottom = reaceiverMessages && scrollViewRef && scrollViewRef.current && scrollViewRef.current.scrollToEnd;
    if (shouldScrollToBottom) scrollViewRef.current.scrollToEnd({animated: true});
  }, [reaceiverMessages]);

  //
  return (
    <View
      style={{
        flex: 1,
        // borderWidth: StyleSheet.hairlineWidth,
        // maxHeight: ht,
      }}>
      {/* <LinearGradient style={{paddingTop: StatusBar.currentHeight}} colors={['#d1b4f5', '#fff']}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            // backgroundColor: isTextArea ? '#2b0757' : '#13a5e0',
            padding: 2,
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: '#fff',
                marginRight: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>{name[0]}</Text>
            </View>

            <View>
              <Text
                style={{
                  color: colors.black,
                  ...typographyVariant.the19,
                  ...typographyWeight.bold,
                }}>
                {name}
              </Text>
            </View>
          </View>
        
        </View>
      </LinearGradient> */}
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <ScrollView
          ref={scrollViewRef}
          bounces={false}
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            padding: 10,
            paddingBottom: 50,
            backgroundColor: '#fff',
          }}>
          {reaceiverMessages.map((message, index) => (
            <Text key={index}>{message}</Text>
          ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default ChatBoxSender;
