import React from 'react';
import {View, Text, NativeModules, LayoutAnimation, Image, Platform, Keyboard, StatusBar, TouchableOpacity} from 'react-native';
import ChatBoxSender from './ChatScreen.Sender';
import ChatBoxReceiver from './ChatScreen.Receiver';
import {useKeyboard, useDimensions} from '@react-native-community/hooks';
import {useSelector} from 'react-redux';
import {useHeaderHeight} from '@react-navigation/stack';
import useCallActions from '../../Helper/useCallActions';
import {getUserInfo} from '../../Store/reduxSelectors';
import messageEvents from '../../Constants/messageEvents';
const {UIManager} = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
const ChatRoom = ({route, navigation}) => {
  const params = route && route.params;

  const {receiverInfo, isChat} = params;
  const headerHeight = useHeaderHeight();
  const userInfo = useSelector(state => getUserInfo(state));

  const [callActions] = useCallActions();

  const keyboardHooks = useKeyboard();

  const dimensions = useDimensions().screen;
  const [keyboardHeight, setKeyboardHeight] = React.useState(0);

  React.useEffect(() => {
    if (Platform.OS === 'android') {
      if (keyboardHooks.keyboardShown) setKeyboardHeight(keyboardHooks.keyboardHeight);
      else setKeyboardHeight(0);
      // LayoutAnimation.easeInEaseOut();
    }
    return () => (Platform.OS === 'android' ? setKeyboardHeight(0) : null);
  }, [keyboardHooks.keyboardShown]);

  React.useEffect(() => {
    if (Platform.OS === 'ios') {
      Keyboard.addListener('keyboardWillShow', _keyboardDidShow);
      Keyboard.addListener('keyboardWillHide', _keyboardDidHide);

      // cleanup function
      return () => {
        Keyboard.removeListener('keyboardWillShow', () => {});
        Keyboard.removeListener('keyboardWillHide', () => {});
      };
    }
  }, []);

  const _keyboardDidShow = e => {
    setKeyboardHeight(e.endCoordinates.height);
  };

  const _keyboardDidHide = e => {
    setKeyboardHeight(0);
  };

  const handleCloseCall = () => {
    navigation.goBack();
    let payload = {
      channelId: receiverInfo.userId,
      event: messageEvents.callHangup,
      from: userInfo.userId,
      to: receiverInfo.userId,
      metadata: {
        senderInfo: userInfo,
      },
    };
    callActions.messageThisUser(payload);
  };

  // React.useLayoutEffect(() => {
  //   if (isChat) {
  //     navigation.setOptions({
  //       headerRight: () => (
  //         <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10}}>
  //           {/* {params && !params.isChat ? <Image style={{width: 20, height: 20, marginRight: 20}} source={require('../../Assets/Images/savetextcall_icon.png')} /> : null} */}
  //           <TouchableOpacity onPress={() => handleCloseCall()}>
  //             <Image style={{width: 20, height: 20, marginRight: 20}} source={require('../../Assets/Images/hangup_icon.png')} />
  //           </TouchableOpacity>
  //           <Image style={{width: 4.5, height: 18}} source={require('../../Assets/Images/threedots.png')} />
  //         </View>
  //       ),
  //     });
  //   }
  // }, [navigation, isChat]);
  React.useLayoutEffect(() => {
    if (isChat) {
      navigation.setOptions({
        headerRight: () => (
          <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10}}>
            {/* {params && !params.isChat ? <Image style={{width: 20, height: 20, marginRight: 20}} source={require('../../Assets/Images/savetextcall_icon.png')} /> : null} */}
            <TouchableOpacity onPress={() => handleCloseCall()}>
              <Image style={{width: 20, height: 20, marginRight: 20}} source={require('../../Assets/Images/hangup_icon.png')} />
            </TouchableOpacity>
            <Image style={{width: 4.5, height: 18}} source={require('../../Assets/Images/threedots.png')} />
          </View>
        ),
        headerLeft: () => (
          <TouchableOpacity style={{paddingHorizontal: 10}} onPress={() => navigation.goBack()}>
            <View flexDirection="row" alignItems="center">
              <Image style={{width: 29 / 2.2, height: 47 / 2.2}} source={require('../../Assets/Images/backarrow.png')} />
              <View
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  backgroundColor: '#fff',
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 10,
                }}>
                <Text>{params.receiverInfo.name && params.receiverInfo.name[0]}</Text>
              </View>
            </View>
          </TouchableOpacity>
        ),
      });
    } else {
      navigation.setOptions({
        headerLeft: () => (
          <TouchableOpacity style={{paddingHorizontal: 10}} onPress={() => navigation.goBack()}>
            <View flexDirection="row" alignItems="center">
              <Image style={{width: 29 / 2.2, height: 47 / 2.2}} source={require('../../Assets/Images/backarrow.png')} />
              <View
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  backgroundColor: '#fff',
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 10,
                }}>
                <Text>{params.receiverInfo.name && params.receiverInfo.name[0]}</Text>
              </View>
            </View>
          </TouchableOpacity>
        ),
      });
    }
  }, [navigation, isChat]);

  return (
    <View style={{flex: 1, backgroundColor: '#fff', maxHeight: dimensions.height - (keyboardHeight + headerHeight)}}>
      <StatusBar translucent backgroundColor="transparent" />
      <ChatBoxReceiver navigation={navigation} name={receiverInfo.name} receiverInfo={receiverInfo} />
      <ChatBoxSender receiverInfo={receiverInfo} />
    </View>
  );
};

export default ChatRoom;
