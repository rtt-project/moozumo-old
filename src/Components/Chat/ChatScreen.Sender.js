import React, {useState} from 'react';
import {Keyboard, View, Text, TouchableOpacity, Dimensions, NativeModules, LayoutAnimation, TextInput, ScrollView, Image, StyleSheet, KeyboardAvoidingView, Button, Platform, ActivityIndicator} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {PubNubProvider, usePubNub} from 'pubnub-react';
import {useSelector} from 'react-redux';
import {getUserInfo} from '../../Store/reduxSelectors';
const {width, height} = Dimensions.get('screen');
import {colors} from '../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../Constants/typography';

const {UIManager} = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const ChatBoxSender = ({receiverInfo}) => {
  const pubnub = usePubNub();
  const userInfo = useSelector(state => getUserInfo(state));

  const sendMessage = (message, event) => {
    if (message)
      pubnub.publish({
        channel: receiverInfo.userId,
        message: JSON.stringify({
          message,
          from: userInfo.userId,
          to: receiverInfo.userId,
          event: event || 'message',
        }),
      });
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
        borderTopWidth: 0.7,
        borderTopColor: '#525252',
        // borderWidth: StyleSheet.hairlineWidth,
        // maxHeight: ht,
      }}>
      <LinearGradient colors={['#0066cc20', '#fff']}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            // backgroundColor: isTextArea ? '#2b0757' : '#13a5e0',
            padding: 2,
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: '#fff',
                marginRight: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>{(userInfo.name && userInfo.name[0]) || (userInfo.userName && userInfo.userName[0])}</Text>
            </View>

            <View>
              <Text
                style={{
                  color: colors.black,
                  ...typographyVariant.the19,
                  ...typographyWeight.bold,
                }}>
                {userInfo.name || userInfo.userName}
              </Text>
            </View>
          </View>
        </View>
      </LinearGradient>

      <View
        style={{
          flex: 1,
        }}>
        <TextInput
          // value={messages[messages.length]}
          style={{
            flex: 1,
            textAlignVertical: 'top',
            paddingBottom: 20,
            padding: 10,
            backgroundColor: '#fff',
          }}
          autoFocus={true}
          multiline={true}
          onChangeText={sendMessage}
          underlineColorAndroid="transparent"
          onSubmitEditing={({text}) => {
            // handleOnChange('nextLine');
          }}
        />
      </View>
    </View>
  );
};

export default ChatBoxSender;
