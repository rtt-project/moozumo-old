import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {colors} from '../../../../Constants/colors';

const StepConnector = ({children}) => {
  return (
    <View style={{flex: 0.5}}>
      <View style={{alignItems: 'center', marginVertical: 2}}>{children}</View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <View style={{backgroundColor: colors.primary, width: 2, height: '100%'}} />
      </View>
    </View>
  );
};

export default StepConnector;
