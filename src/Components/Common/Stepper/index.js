import React from 'react';
import {View} from 'react-native';

const Stepper = ({children}) => {
  return <View style={{flexDirection: 'row', alignItems: 'center'}}>{children}</View>;
};

export default Stepper;
