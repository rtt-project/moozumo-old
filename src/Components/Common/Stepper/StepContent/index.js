import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {colors} from '../../../../Constants/colors';

const StepContent = ({children}) => {
  return <View style={{flex: 2, padding: 10}}>{children}</View>;
};

export default StepContent;
