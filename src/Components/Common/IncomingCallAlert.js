import React from 'react';
import {View, Text, SafeAreaView, StatusBar} from 'react-native';
import CallScreen from '../Call/CallScreen';
const IncomingCallAlert = (props) => {
  return (
    <View style={{flex: 1}}>
      <StatusBar translucent backgroundColor="transparent" />
      <CallScreen isIncomingCall={true} {...props} />
    </View>
  );
};

export default IncomingCallAlert;
