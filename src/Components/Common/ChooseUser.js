import React from 'react';
import {Button, Text, View, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Contacts from '../../Store/localData/ContactItems.json';
const ChooseUser = ({onSelect}) => {
  const [currentUser, setCurrentUser] = React.useState(Contacts[0]);

  return (
    <View>
      <View
        style={{
          justifyContent: 'center',
          backgroundColor: '#fff',
          padding: 15,
          margin: 20,
        }}>
        <Text style={{fontSize: 20, ...typographyWeight.bold,}}>Please select a User</Text>

        <Picker selectedValue={currentUser && currentUser.id} onValueChange={(itemValue, itemIndex) => setCurrentUser(Contacts[itemIndex])}>
          {Contacts.map((item, index) => (
            <Picker.Item key={index} label={item.name} value={item.id} />
          ))}
        </Picker>
        <Button title="Select" onPress={() => onSelect(currentUser)} />
      </View>
    </View>
  );
};

export default ChooseUser;
