import React from 'react';
import {TouchableOpacity, ActivityIndicator, Text} from 'react-native';
import {colors} from '../../Constants/colors';

const getVariantStyles = {
  outlined: {
    backgroundColor: '#fff',
    borderColor: '#696969',
    borderWidth: 2,
  },
};

const CustomButton = ({title, variant, onPress, isLoading, disabled}) => {
  const textColor = variant === 'outlined' ? '#696969' : '#fff';

  return (
    <TouchableOpacity
      disabled={isLoading || disabled}
      onPress={onPress}
      style={{
        padding: 10,
        backgroundColor: colors.primary,
        // minHeight: 60,
        // borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        ...(getVariantStyles[variant] || {}),
      }}>
      <Text
        style={{
          ...typographyWeight.bold,
          color: textColor,
        }}>
        {isLoading ? <ActivityIndicator color={textColor} /> : title}
      </Text>
    </TouchableOpacity>
  );
};

export default CustomButton;
