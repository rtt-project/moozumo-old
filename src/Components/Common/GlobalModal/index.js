import React from 'react';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getModalProps} from '../../../Store/reduxSelectors';
import Modal from 'react-native-modal';
import IncomingCallAlert from '../IncomingCallAlert';

const GlobalModal = () => {
  const modalProps = useSelector((state) => getModalProps(state));
  return (
    <Modal style={{margin: 0}} animationOut="fadeOut" animationIn="fadeIn" isVisible={modalProps.isVisible}>
      {modalProps.component || <View></View>}

      {/* <IncomingCallAlert /> */}
    </Modal>
  );
};

export default GlobalModal;
