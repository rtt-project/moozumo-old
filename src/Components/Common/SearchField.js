import React from 'react';
import {View, Image, TextInput, StyleSheet} from 'react-native';

const SearchField = ({placeHolder, value, onChangeText}) => {
  return (
    <View style={styles.input}>
      <Image style={{height: 19.5, width: 20}} source={require('../../Assets/Images/serach_user.png')} />
      <TextInput style={{marginLeft: 10, flex: 1}} onChangeText={onChangeText} placeholder={placeHolder ? placeHolder :"[Search]"} value={value} />
    </View>
  );
};

export default SearchField;

const styles = StyleSheet.create({
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    height: 50,
    marginVertical: 12,
    borderWidth: 1,
    borderColor: '#00000025',
    color: '#00000080',
  },
});
