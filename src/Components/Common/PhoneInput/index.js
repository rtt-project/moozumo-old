import React from 'react';
import {View, Text, FlatList, StyleSheet, Pressable, Button, TouchableOpacity, SafeAreaView, TextInput} from 'react-native';
import {TextField} from 'rn-material-ui-textfield';
import {Controller} from 'react-hook-form';
import PhoneNumber from 'awesome-phonenumber';
import Modal from 'react-native-modal';

import countryCode from '../../../Constants/countryCodes.json';
import SearchField from '../SearchField';
import {colors} from '../../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../../Constants/typography';
import CustomInput from '../CustomInput';
// import {TouchableOpacity} from 'react-native-gesture-handler';

const PhoneInput = ({watch, control, register, errors, isLoading, setValue}: any) => {
  console.log('errors: ', errors);
  const [selectedCountryCode, setSelectedCountryCode] = React.useState({code: 'US', emoji: '🇺🇸', name: 'United States', unicode: 'U+1F1FA U+1F1F8'});
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [searchByCountry, setSearchByCountry] = React.useState('');

  React.useEffect(() => {
    register('countryCode', {required: true});
  }, [register]);

  const [watchPhoneNumber, watchCountryCode] = watch(['phoneNumber', 'countryCode']);

  const filteredItems = item => item.name.toLocaleLowerCase().includes(searchByCountry.toLocaleLowerCase());

  React.useEffect(() => {
    const countryCodeForRegionCode = '+' + PhoneNumber.getCountryCodeForRegionCode(selectedCountryCode.code);
    setValue('countryCode', countryCodeForRegionCode);
  }, [selectedCountryCode]);

  return (
    <View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View style={{marginRight: 30, position: 'relative'}}>
          {/* <Button color={colors.black} title={`${selectedCountryCode.emoji} ${watchCountryCode}`} onPress={() => setIsModalOpen(true)} /> */}
          <Pressable style={{paddingHorizontal: 15, paddingVertical: 8, borderBottomWidth: StyleSheet.hairlineWidth, justifyContent: 'center', alignItems: 'center', marginTop: 20}} onPress={() => setIsModalOpen(true)}>
            <Text style={{...typographyVariant.the14}}>{`${selectedCountryCode.emoji} ${watchCountryCode}`}</Text>
          </Pressable>
        </View>

        <View style={{flex: 1, overflow: 'hidden'}}>
          <View>
            {/* <CustomInput
              textInputProps={{
                keyboardType: 'phone-pad',
              }}
              rules={{
                validate: value => {
                  var pn = PhoneNumber(watchPhoneNumber, selectedCountryCode.code);
                  return pn.isMobile();
                },
              }}
              control={control}
              name="phoneNumber"
              label={'Phone Number*'}
              keyboardType="phone-pad"
              editable={!isLoading}
            /> */}
            <Controller
              name="phoneNumber"
              control={control}
              rules={{
                required: true,
                validate: value => {
                  if (value) {
                    var pn = PhoneNumber(watchPhoneNumber, selectedCountryCode.code);
                    return pn.isMobile();
                  } else return true;
                },
              }}
              render={({field: {onChange, onBlur, value, name, ref}, fieldState: {invalid, isTouched, isDirty, error}, formState}) => <TextField maxLength={10} keyboardType="phone-pad" error={errors.phoneNumber ? (errors.phoneNumber.type === 'validate' ? 'Invalid Phone Number' : 'This Field Is Required') : ''} editable={!isLoading} label={'Phone Number*'} value={value} onChangeText={value => onChange(value)} onBlur={onBlur} />}
            />
          </View>
        </View>
      </View>

      <Modal isVisible={isModalOpen} style={{margin: 0}}>
        <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
          <View style={{flex: 1, backgroundColor: colors.white}}>
            <FlatList
              keyboardShouldPersistTaps="always"
              stickyHeaderIndices={[0]}
              ListHeaderComponent={
                <View style={{paddingHorizontal: 10, backgroundColor: colors.white}}>
                  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <Text style={{...typographyVariant.the17, ...typographyWeight.bold}}>Choose Country Code</Text>
                    <TouchableOpacity
                      onPress={() => {
                        setSearchByCountry('');
                        setIsModalOpen(false);
                      }}>
                      <Text style={{...typographyVariant.the14, ...typographyWeight.bold}}>Close</Text>
                    </TouchableOpacity>
                  </View>
                  <SearchField placeHolder="Search Country name" value={searchByCountry} onChangeText={value => setSearchByCountry(value)} />
                </View>
              }
              data={countryCode.filter(item => filteredItems(item))}
              renderItem={({item}) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSelectedCountryCode(item);
                      setIsModalOpen(false);
                      setSearchByCountry('');
                    }}
                    style={{paddingHorizontal: 10, paddingVertical: 15}}>
                    <Text style={[{...typographyVariant.the15PinchMore}, selectedCountryCode.code === item.code ? {...typographyWeight.bold} : {}]}>{`${item.emoji} ${item.name}`}</Text>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={item => item.code}
            />
          </View>
        </SafeAreaView>
      </Modal>
    </View>
  );
};

export default PhoneInput;
