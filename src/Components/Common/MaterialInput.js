import React from 'react';
import {View} from 'react-native';
import {Controller} from 'react-hook-form';
import {TextField} from 'rn-material-ui-textfield';

const MaterialInput = ({errorMessage = '', name = 'test', error, defaultValue = '', required = true, rootStyles = {}, editable = true, label, control, textInputProps = {}, controllerRules = {}}) => {
  return (
    <View style={{...rootStyles}}>
      <Controller control={control} render={({field: {onChange, onBlur, value, name, ref}, fieldState: {invalid, isTouched, isDirty, error}, formState}) => <TextField maxLength={50} error={error ? (errorMessage ? errorMessage : 'This Field Is Required') : ''} editable={editable} label={label} value={value} onChangeText={value => onChange(value)} onBlur={onBlur} {...textInputProps} />} name={name} rules={{required, ...controllerRules}} defaultValue={defaultValue} />
    </View>
  );
};

export default MaterialInput;
