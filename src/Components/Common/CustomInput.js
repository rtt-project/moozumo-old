import React from 'react';
import {TextInput, Text, View} from 'react-native';
import {Controller} from 'react-hook-form';
import {typographyVariant, typographyWeight} from '../../Constants/typography';
// import normalize from '../../Helper/normalize';

const errorStyles = {
  borderColor: 'red',
  borderBottomWidth: 2,
};
const CustomTextInput = ({name = 'test', error, defaultValue = '', rules = {}, required = true, rootStyles = {}, editable = true, label, control, textInputProps = {}}) => {
  return (
    <View style={{...rootStyles}}>
      {label && (
        <Text
          style={{
            paddingVertical: 5,
            // fontSize: normalize(1.8),
            color: '#333',
            textTransform: 'capitalize',
            ...typographyVariant.the12,
          }}>
          {label}
        </Text>
      )}
      <Controller
        control={control}
        render={({field: {onChange, onBlur, value, name, ref}, fieldState: {invalid, isTouched, isDirty, error}, formState}) => (
          <TextInput
            editable={editable}
            style={[
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                minHeight: 35,
                color: '#000',
                ...typographyVariant.the20,
              },
              error
                ? errorStyles
                : {
                    borderBottomWidth: 2,
                    borderColor: '#eee',
                  },
            ]}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
            {...textInputProps}
          />
        )}
        name={name}
        rules={{required, ...rules}}
        defaultValue={defaultValue}
      />
    </View>
  );
};

export default CustomTextInput;
