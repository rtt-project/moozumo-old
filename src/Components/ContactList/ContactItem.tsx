import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';

const ContactItem = ({title, item}: {item: any; title: string}) => {
  return (
    <View style={[styles.item, {backgroundColor: item.userId ? 'transparent' : colors.greybg}]}>
      <View
        style={{
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: colors.primary,
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: 10,
          marginLeft: 2,
          borderWidth: item.userId ? 1 : 0,
          borderColor: 'green',
        }}>
        <Text
          style={{
            textTransform: 'capitalize',
            ...typographyVariant.the15,
            color: colors.white,
          }}>
          {title && title[0]}
        </Text>
      </View>
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <View style={{flex: 1}}>
          <Text numberOfLines={1} style={{...typographyVariant.the15}}>
            {title}
          </Text>
          <Text numberOfLines={1} style={{color: colors.offlinegray, ...typographyVariant.the12}}>
            {`${item.countryCode} ${item.phoneNumber}`}
          </Text>
        </View>
        {item.isPinnedContact ? (
          <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
            <Image style={{width: 20, height: 20}} source={require('../../Assets/Images/pin.png')} />
          </View>
        ) : null}
        {item.userId ? null : (
          <TouchableOpacity disabled={!!invitingUser} onPress={() => handleInviteContacts(item)}>
            <View style={{paddingHorizontal: 10, flexDirection: 'column', alignItems: 'center'}}>
              {invitingUser === item.countryCode + item.phoneNumber ? (
                <ActivityIndicator color={colors.black} />
              ) : (
                <>
                  <Image style={{width: 22, height: 32 / 2}} source={require('../../Assets/Images/addcontact.png')} />
                  <Text style={{...typographyVariant.the12, color: colors.black}}>Invite</Text>
                </>
              )}
            </View>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default ContactItem;
