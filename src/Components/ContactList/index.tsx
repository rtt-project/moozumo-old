import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet, Dimensions, SafeAreaView, FlatList, StatusBar, TextInput, ActivityIndicator} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {colors} from '../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../Constants/typography';
import {getUserContactsFromState, getUserInfo, contactListLoading, deviceContactLoading, getContactCountsFromState} from '../../Store/reduxSelectors';
import {getContacts, getLatestContactsFromDevice, getContactsFromDevice, requsetPermissionToReadContact, searchContacts, inviteContacts, getContactsFromLocalDatabase} from '../../Store/Actions/Contacts';
import useCallActions from '../../Helper/useCallActions';
import {messagePayload, senderInfoType} from '../../Constants/types';
import messageEvents from '../../Constants/messageEvents';

const {width, height} = Dimensions.get('window');

const ContactList = ({route, navigation}: any) => {
  const Item = ({title, item}: any) => (
    <View style={[styles.item, {backgroundColor: item.userId ? 'transparent' : colors.greybg}]}>
      <View
        style={{
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: colors.primary,
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: 10,
          marginLeft: 2,
          borderWidth: item.userId ? 1 : 0,
          borderColor: 'green',
        }}>
        <Text
          style={{
            textTransform: 'capitalize',
            ...typographyVariant.the15,
            color: colors.white,
          }}>
          {title && title[0]}
        </Text>
      </View>
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <View style={{flex: 1}}>
          <Text numberOfLines={1} style={{...typographyVariant.the15}}>
            {title}
          </Text>
          <Text numberOfLines={1} style={{color: colors.offlinegray, ...typographyVariant.the12}}>
            {`${item.countryCode} ${item.phoneNumber}`}
          </Text>
        </View>
        {item.isPinnedContact ? (
          <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
            <Image style={{width: 20, height: 20}} source={require('../../Assets/Images/pin.png')} />
          </View>
        ) : null}
        {item.userId ? null : (
          <TouchableOpacity disabled={!!invitingUser} onPress={() => handleInviteContacts(item)}>
            <View style={{paddingHorizontal: 10, flexDirection: 'column', alignItems: 'center'}}>
              {invitingUser === item.countryCode + item.phoneNumber ? (
                <ActivityIndicator color={colors.black} />
              ) : (
                <>
                  <Image style={{width: 22, height: 32 / 2}} source={require('../../Assets/Images/addcontact.png')} />
                  <Text style={{...typographyVariant.the12, color: colors.black}}>Invite</Text>
                </>
              )}
            </View>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );

  const dispatch = useDispatch();

  const [searchKey, onChangeSearchKey] = React.useState('');
  const [invitingUser, setInvitingUser] = React.useState('');
  const [pagination, setPagination] = React.useState({
    page: 1,
    limit: 50,
  });
  const [isPagination, setIsPagination] = React.useState(false);

  const userContacts = useSelector(state => getUserContactsFromState(state));

  const loading = useSelector(state => contactListLoading(state));

  const userInfo = useSelector(state => getUserInfo(state));
  const deiveContactLoading = useSelector(state => deviceContactLoading(state));
  const counts = useSelector(state => getContactCountsFromState(state));

  const handleAddToContact = () => {
    navigation.push('CreateContact');
  };
  const handleContactClick = (contactDetails: any) => {
    navigation.push('ContactDetails', {contactDetails});
  };

  React.useEffect(() => {
    _componentDidMount();
  }, []);

  const _componentDidMount = async () => {
    dispatch(getContacts(pagination));
  };

  const [callActions] = useCallActions();

  React.useEffect(() => {
    if (loading) return;
    else dispatch(searchContacts(searchKey, pagination));
  }, [searchKey]);

  const handleCallUser = (senderInfo: senderInfoType) => {
    if (senderInfo.userId) {
      let payload: messagePayload = {
        channelId: senderInfo.userId,
        event: messageEvents.incomingCall,
        from: userInfo.userId,
        to: senderInfo.userId,
        metadata: {
          senderInfo: userInfo,
        },
      };
      callActions.callThisUser(payload, senderInfo);
    }
  };

  const EmptyListMessage = () => {
    return (
      <View style={{height: height * 0.7, flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
        <Image style={{width: 100, height: 100}} source={require('../../Assets/Images/largeicon_nocintact.png')} />
        <Text
          style={{
            ...typographyVariant.the20,
            ...typographyWeight.bold,
          }}>
          {`No Contacts ${searchKey ? 'Found' : 'Yet'}`}
        </Text>
      </View>
    );
  };

  const renderItem = ({item}: {item: any}) => (
    <View style={{marginBottom: 1, opacity: item.userId ? 1 : 0.8}}>
      <TouchableOpacity disabled={!item.userId} onPress={() => handleContactClick(item)}>
        {item.userId ? (
          <Swipeable key={item.name + item.userId} renderRightActions={(progressAnimatedValue, dragAnimatedValue) => renderRightActions(progressAnimatedValue, dragAnimatedValue, item)}>
            <Item title={item.name} item={item} />
          </Swipeable>
        ) : (
          <Item title={item.name} item={item} />
        )}
      </TouchableOpacity>
    </View>
  );

  const handleManualRefresh = async () => {
    await dispatch(getLatestContactsFromDevice());
  };

  const handleInviteContacts = async (contacts: any) => {
    setInvitingUser(contacts && contacts.countryCode + contacts.phoneNumber);
    await dispatch(inviteContacts([contacts]));
    setInvitingUser('');
  };

  const renderRightActions = (progress: any, dragX: any, item: senderInfoType) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
    });

    return (
      <>
        <TouchableOpacity
          onPress={() => handleCallUser(item)}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/text_call.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontSize: 8,
            }}>
            Text Call
          </Text>
          <View
            style={{
              borderLeftColor: colors.white,
              borderLeftWidth: 0.2,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => callActions.redirectToChatScreen(item, false)}
          activeOpacity={0.8}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/message_icon.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontSize: 8,
            }}>
            Message
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  const handlePagination = async () => {
    if (isPagination || searchKey !== '') null;
    else {
      const shouldFetchForMore = userContacts.length < counts.total;
      if (shouldFetchForMore) {
        setIsPagination(true);
        let _pagination = {...pagination};
        _pagination.page = _pagination.page + 1;
        setPagination(_pagination);
        await dispatch(getContactsFromLocalDatabase(_pagination));
        setIsPagination(false);
      }
    }

    // dispatch(updatePagination());
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{...typographyVariant.the12, ...typographyWeight.bold}}>Getting User Contacts</Text>
          <ActivityIndicator color={colors.black} />
        </View>
      ) : (
        <View style={{flex: 1}}>
          {userContacts.length === 0 && deiveContactLoading ? (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{...typographyVariant.the12, ...typographyWeight.bold}}>This is one time process, it may take some time.</Text>
              <ActivityIndicator color={colors.black} />
            </View>
          ) : (
            <React.Fragment>
              <View style={styles.input}>
                <Image style={{height: 19.5, width: 20}} source={require('../../Assets/Images/serach_user.png')} />
                <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                  <TextInput style={{marginLeft: 10, flex: 1}} onChangeText={onChangeSearchKey} placeholder="[Search my contacts]" value={searchKey} />
                  {searchKey ? (
                    <TouchableOpacity style={{padding: 5}} onPress={() => onChangeSearchKey('')}>
                      <Text style={{...typographyVariant.the12, ...typographyWeight.bold}}>Clear</Text>
                    </TouchableOpacity>
                  ) : null}
                </View>
              </View>
              <TouchableOpacity onPress={() => handleAddToContact()}>
                <View style={styles.addContact}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image style={styles.tinyLogo} source={require('../../Assets/Images/addcontactplus.png')} />
                    <Text style={{...typographyVariant.the32, ...typographyWeight.bold}}>Add to contact</Text>
                  </View>
                  <Text style={{...typographyVariant.the32, ...typographyWeight.bold}}>{`${counts.moozumoUser} (${counts.total})`}</Text>
                </View>
              </TouchableOpacity>
              <FlatList
                ListFooterComponent={
                  <View>
                    {isPagination ? (
                      <View style={{justifyContent: 'center', alignItems: 'center', paddingVertical: 20}}>
                        <Text style={{...typographyVariant.the12, ...typographyWeight.bold, textAlign: 'center'}}>Loading Contacts...</Text>
                        <ActivityIndicator color={colors.black} />
                      </View>
                    ) : null}
                  </View>
                }
                onEndReached={() => handlePagination()}
                onEndReachedThreshold={1}
                contentContainerStyle={{paddingBottom: 100}}
                data={userContacts}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={<EmptyListMessage />}
              />
            </React.Fragment>
          )}
        </View>
      )}

      {loading ? null : (
        <TouchableOpacity disabled={deiveContactLoading} onPress={() => handleManualRefresh()} style={{position: 'absolute', justifyContent: 'center', alignItems: 'center', bottom: 10, right: 10, minWidth: width / 2.5, paddingHorizontal: 20, paddingVertical: 10, backgroundColor: colors.primary, borderRadius: 100}}>
          {deiveContactLoading ? <ActivityIndicator size={18} color={colors.white} /> : <Text style={{...typographyVariant.the12, color: colors.white}}>Refresh Contacts</Text>}
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    position: 'relative',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#00000008',
    // height: 50,
    // flexDirection: 'row',
    // alignItems: 'center',
    // backgroundColor: '#fff',
    // padding: 20,
    // marginHorizontal: 16,
  },
  addContact: {
    // height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#00000025',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 14,
  },
  tinyLogo: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    marginRight: 10,
  },
  logo: {
    width: 66,
    height: 58,
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    height: 50,
    marginVertical: 12,
    borderWidth: 1,
    borderColor: '#00000025',
    color: '#00000080',
    marginHorizontal: 16,
  },
});
export default ContactList;
