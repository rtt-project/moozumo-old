import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import ContactItem from './ContactItem';
import {messagePayload, senderInfoType} from '../../Constants/types';
import {colors} from '../../Constants/colors';

const ContactItemWrapper = ({item, handleContactClick, handleCallUser, callActions}: {item: any; handleContactClick: Function; handleCallUser: Function; callActions: Object}) => {
  const renderRightActions = (progress: any, dragX: any, item: senderInfoType) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
    });

    return (
      <>
        <TouchableOpacity
          onPress={() => handleCallUser(item)}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/text_call.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontSize: 8,
            }}>
            Text Call
          </Text>
          <View
            style={{
              borderLeftColor: colors.white,
              borderLeftWidth: 0.2,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => callActions.redirectToChatScreen(item, false)}
          activeOpacity={0.8}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/message_icon.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontSize: 8,
            }}>
            Message
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  return (
    <View style={{marginBottom: 1, opacity: item.userId ? 1 : 0.8}}>
      <TouchableOpacity disabled={!item.userId} onPress={() => handleContactClick(item)}>
        {
          item.userId ? (
            <Swipeable key={item.name + item.userId} renderRightActions={(progressAnimatedValue, dragAnimatedValue) => renderRightActions(progressAnimatedValue, dragAnimatedValue, item)}>
              {/* <ContactItem title={item.name} item={item} /> */}
            </Swipeable>
          ) : null
          // <ContactItem title={item.name} item={item} />
        }
      </TouchableOpacity>
    </View>
  );
};

export default ContactItemWrapper;
