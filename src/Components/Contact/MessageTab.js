import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import MessageContactItem from './MessageContactItem';
import MessageContactItems from './../../Store/localData/ContactItems.json';

const MessageTab = () => {
  return (
    <View>
      <FlatList
        Keyextractor={(MessageContactItem) => MessageContactItem.name}
        data={MessageContactItems}
        renderItem={({item, index}) => {
          return (
            <MessageContactItem data={item} index={index}></MessageContactItem>
          );
        }}
      />
    </View>
  );
};

const MessageTabStyle = StyleSheet.create({
  flatlistContainer: {},
});
export default MessageTab;
