import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {formatDistance, subDays} from 'date-fns';

const MessageContactItem = ({data, index}) => {
  let status = getStatusText({data});
  let statusColor = getStatusColor({data});
  let displayDate = getDisplayDate({data});

  return (
    <View
      style={
        index % 2 === 1
          ? MessageListItemStyle.container
          : MessageListItemStyle.containerAlt
      }>
      <Image
        source={{
          height: 35,
          width: 35,
          borderRadius: 17,
          uri: data.profilePic,
        }}></Image>
      <View
        style={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: statusColor,
        }}></View>
      <Text>{data.name}</Text>
      <Text>{status}</Text>
      <Text>{displayDate}</Text>
    </View>
  );
};

const getDisplayDate = ({data}) => {
  return formatDistance(data.lastSeen, new Date(), {addSuffix: false});
};

const getStatusColor = ({data}) => {
  let statusColor;
  switch (data.status) {
    case 1:
      statusColor = 'green';
      break;
    case 2:
      statusColor = 'orange';
      break;
    case 3:
      statusColor = 'grey';
      break;
    default:
      statusColor = 'white';
      break;
  }
  return statusColor;
};

const getStatusText = ({data}) => {
  let statusText;
  switch (data.status) {
    case 1:
      statusText = 'Available';
      break;
    case 2:
      statusText = 'Away';
      break;
    case 3:
      statusText = 'Offline';
      break;
    default:
      statusText = 'Missing';
      break;
  }
  return statusText;
};
const MessageListItemStyle = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 35,
    borderRadius: 5,
  },
  containerAlt: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 35,
    borderRadius: 5,
    backgroundColor: 'lightgrey',
  },
});
export default MessageContactItem;
