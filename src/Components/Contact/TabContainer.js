import * as React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import TextCallTab from './TextCallTab';
import MessageTab from './MessageTab';

const initialLayout = {width: Dimensions.get('window').width};

export default function TabContainer() {
  const [index, setIndex] = React.useState(1);
  const [routes] = React.useState([
    {key: 'message', title: 'Message'},
    {key: 'text', title: 'Text Call'},
  ]);
  const renderScene = SceneMap({
    message: MessageTab,
    text: TextCallTab,
  });

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}></TabView>
  );
}
const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});
