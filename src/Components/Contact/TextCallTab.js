import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import TextContactItem from './TextContactItem';
import ContactItems from './../../Store/localData/ContactItems.json';

const TextCallTab = () => {
  return (
    <View>
      <FlatList
        Keyextractor={(TextContactItem) => TextContactItem.name}
        data={ContactItems}
        renderItem={({item, index}) => {
          return <TextContactItem data={item} index={index}></TextContactItem>;
        }}
      />
    </View>
  );
};

const TextTabStyle = StyleSheet.create({});

export default TextCallTab;
