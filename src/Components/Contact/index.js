import React from 'react';
import {View, StyleSheet, FlatList, Text} from 'react-native';
// import SearchBar from './SearchBar';
// import TabContainer from './TabContainer';

const contacts = [{name: 'Areeb'}, {name: 'Majid'}];

const ContactScreen = () => {
  return (
    <View style={{flex: 1}}>
      <FlatList
        data={contacts}
        renderItem={({item}) => (
          <View style={{padding: 20, backgroundColor: 'yellow'}}>
            <Text>{item.name}</Text>
          </View>
        )}
        keyExtractor={(_, index) => index.toString()}
      />
      {/* <SearchBar></SearchBar>
      <TabContainer></TabContainer> */}
    </View>
  );
};
const chatStyle = StyleSheet.create({
  contactlistContainer: {},
});

export default ContactScreen;
