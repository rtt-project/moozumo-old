import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
// import AntDesign from 'react-native-vector-icons/AntDesign';

const SearchBar = () => {
  return (
    <View style={SearchBarstyles.searchbarContainer}>
      <View style={SearchBarstyles.iconContainerStyle}>{/* <AntDesign name="user" size={35}></AntDesign> */}</View>
      <TextInput placeholder="[Search contacts]" style={SearchBarstyles.textStyle}></TextInput>
    </View>
  );
};

const SearchBarstyles = StyleSheet.create({
  searchbarContainer: {
    borderRadius: 15,
    marginHorizontal: 15,
    backgroundColor: 'white',
    height: 50,
    borderColor: 'lightgrey',
    borderWidth: 2,
    flexDirection: 'row',
  },
  textStyle: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  iconContainerStyle: {
    backgroundColor: 'lightgrey',
  },
});

export default SearchBar;
