//
// 2021-05-01 - Aswini - Init File - from ContactList
// 2021-05-02 - Aswini - static json, Basic style for Item
//

import React from 'react';
import {Animated, View, Text, TouchableOpacity, Image, StyleSheet, Dimensions, SafeAreaView, FlatList, StatusBar, TextInput} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import {useSelector, useDispatch} from 'react-redux';
import {usePubNub} from 'pubnub-react';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {useIsFocused} from '@react-navigation/native';

import {colors} from '../../Constants/colors';
import {typographyVariant, typographyWeight} from '../../Constants/typography';
import {getUserContactsFromState, getUserInfo} from '../../Store/reduxSelectors';
import {getContacts} from '../../Store/Actions/Contacts';
import {logOutUser} from '../../Store/Actions/Auth';
import {toggleModal} from '../../Store/Actions/Global';
import IncomingCallAlert from '../Common/IncomingCallAlert';
import messageEvents from '../../Constants/messageEvents';

// Data from local json file
import TextTabItemsFromLocalJson from '../../Store/localData/TextTabItems.json';

const {width, height} = Dimensions.get('window');

const CALL_ACTION_TYPES = {
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_REJECTED: 'CALL_REJECTED',
};

const Item = ({title, item}) => (
  <View style={styles.item}>
    <View style={{flexDirection: 'row'}}>
      <View
        style={{
          height: 40,
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: 10,
        }}>
        {/* <Text
        style={{
          textTransform: 'capitalize',
          ...typographyVariant.the15,
          color: colors.white,
        }}>
        {title[0]}
      </Text> */}
        <Image style={styles.imageStyle} source={{uri: item.profilePic}} />
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: item.id == 1 ? colors.activegreen : item.id == 2 ? colors.awayyellow : colors.offlinegray,
            position: 'absolute',
            top: 0,
            right: 0,
          }}></View>
      </View>
      <View style={{justifyContent: 'center'}}>
        <Text style={{...typographyVariant.the15, color: colors.black}}>{title}</Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {item.id == 1 && <Image style={{width: 28 / 2, height: 17 / 2, marginRight: 3}} source={require('../../Assets/Images/missedcall.png')} />}
          {item.id == 2 && <Image style={{width: 11, height: 11, marginRight: 3}} source={require('../../Assets/Images/incoming.png')} />}
          {item.id != 1 && item.id != 2 && <Image style={{width: 11, height: 11, marginRight: 3}} source={require('../../Assets/Images/outgoing.png')} />}
          <Text style={{...typographyVariant.the12, color: colors.black}}>{item.id == 1 ? '(03) 45 min ago' : item.id == 2 ? 'Yesterday 2.36 pm' : '22 March'}</Text>
          <Text style={{...typographyVariant.the12, color: colors.secondery}}>{item.id == 2 ? ' [Call saved]' : ''}</Text>
        </View>
      </View>
    </View>
    <View style={{justifyContent: 'center', alignContent: 'center', alignContent: 'center', alignItems: 'center'}}>
      {item.id == 2 && <Image style={{width: 20, height: 20}} source={require('../../Assets/Images/savetext_message.png')} />}
      <Text style={{...typographyVariant.the12, color: colors.black}}>{item.id == 2 ? 'Saved call' : ''}</Text>
    </View>
  </View>
);

// renderLeftActions = (progress, dragX) => {
//     const trans = dragX.interpolate({
//         inputRange: [0, 50, 100, 101],
//         outputRange: [-20, 0, 0, 1],
//     });
//     return (
//         <RectButton style={styles.leftAction} onPress={this.close}>
//             <Animated.Text
//                 style={[
//                     styles.actionText,
//                     {
//                         transform: [{ translateX: trans }],
//                     },
//                 ]}>
//                 Archive
//         </Animated.Text>
//         </RectButton>
//     );
// };

const TextTab = ({route, navigation}) => {
  const dispatch = useDispatch();
  const pubnub = usePubNub();
  const isFocused = useIsFocused();

  const [text, onChangeText] = React.useState('');
  const [callLog, setCallLog] = React.useState({});
  const callLogRef = React.useRef(null);
  const isFocusedRef = React.useRef(null);

  callLogRef.current = callLog;
  isFocusedRef.current = isFocused;

  const userContacts = useSelector(state => getUserContactsFromState(state));
  const userInfo = useSelector(state => getUserInfo(state));

  // const handleAddToContact = () => {
  //   navigation.push('CreateContact');
  // };

  React.useEffect(() => {
    _componentDidMount();
  }, []);

  _componentDidMount = async () => {
    // dispatch(getContacts());
  };

  React.useEffect(() => {
    // pubnub.subscribe({ channels: [userInfo.userId] });
    // pubnub.addListener({ message: handleMessage });
    // return () => {
    //   removeAllPubnub();
    // };
  }, [pubnub]);

  const removeAllPubnub = () => {
    pubnub.unsubscribeAll();
    pubnub.removeListener();
  };
  // Message Recieving Section

  const handleMessage = event => {
    const payload = JSON.parse(event && event.message);
    switch (payload.event) {
      case messageEvents.makeACall:
        setCallLog(payload && payload.metadata && payload.metadata.userInfo);
        dispatch(toggleModal(true, <IncomingCallAlert onDecline={receiverInfo => handleCallAction(CALL_ACTION_TYPES.CALL_REJECTED, receiverInfo)} onAccept={receiverInfo => handleCallAction(CALL_ACTION_TYPES.CALL_ACCEPTED, receiverInfo)} data={payload && payload.metadata && payload.metadata.userInfo} />));
        break;
      case messageEvents.callCancelled:
        if (callLogRef && callLogRef.current && callLogRef.current.userId === payload.from) dispatch(toggleModal(false, null));
        break;
      case messageEvents.callAccepted:
        if (isFocusedRef.current) {
          handleOnMessage(payload && payload.metadata && payload.metadata.userInfo);
          dispatch(toggleModal(false, null));
        }
        break;
    }
  };

  const handleMakeACall = receiverInfo => {
    let payload = {
      channelId: receiverInfo.userId,
      from: userInfo.userId,
      to: receiverInfo.userId,
      event: messageEvents.makeACall,
      metadata: {
        userInfo,
      },
    };
    setCallLog(receiverInfo);
    pubnub.publish({
      channel: receiverInfo.userId,
      message: JSON.stringify(payload),
    });
    dispatch(toggleModal(true, <IncomingCallAlert data={receiverInfo} isIncomingCall={false} onDecline={payload => handleCallAction(CALL_ACTION_TYPES.CALL_REJECTED, payload)} />));
  };

  const handleCallAction = (callActionType, receiverInfo) => {
    let payload = {
      channelId: receiverInfo.userId,
      from: userInfo.userId,
      to: receiverInfo.userId,
      metadata: {
        userInfo,
      },
    };
    switch (callActionType) {
      case CALL_ACTION_TYPES.CALL_REJECTED:
        payload.event = messageEvents.callCancelled;
        pubnub.publish({
          channel: receiverInfo.userId,
          message: JSON.stringify(payload),
        });
        break;

      case CALL_ACTION_TYPES.CALL_ACCEPTED:
        payload.event = messageEvents.callAccepted;
        pubnub.publish({
          channel: receiverInfo.userId,
          message: JSON.stringify(payload),
        });
        handleOnMessage(receiverInfo);
        break;
    }
    dispatch(toggleModal(false, null));
  };
  const renderItem = ({item}) => (
    <View style={{marginBottom: 1}}>
      {/* <Swipeable  renderRightActions={(progressAnimatedValue, dragAnimatedValue) => renderRightActions(progressAnimatedValue, dragAnimatedValue, item)}> */}
      <Item title={item.name} item={item} />
      {/* </Swipeable> */}
    </View>
  );
  const handleOnMessage = receiverInfo => {
    navigation.push('ChatScreen', {receiverInfo});
  };

  // headerRight: () => (

  // ),
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={{paddingHorizontal: 20}} onPress={() => dispatch(logOutUser())}>
          <Image style={{width: 4.8, height: 18.4}} source={require('../../Assets/Images/threedots.png')} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);
  const renderRightActions = (progress, dragX, item) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
    });
    return (
      <>
        <TouchableOpacity
          onPress={() => handleMakeACall(item)}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/text_call.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              ...typographyVariant.the12,
            }}>
            Text Call
          </Text>
          <View
            style={{
              borderLeftColor: colors.white,
              borderLeftWidth: 0.2,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleOnMessage(item)}
          activeOpacity={0.8}
          style={{
            backgroundColor: colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{width: 25, height: 25}} source={require('../../Assets/Images/message_icon.png')} />
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              ...typographyVariant.the12,
            }}>
            Message
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1}}>
      <View style={styles.input}>
          <Image style={{ height: 19.5, width: 20 }} source={require('../../Assets/Images/serach_user.png')} />
          <TextInput style={{ marginLeft: 10 }} onChangeText={onChangeText} placeholder="[Search]" value={text} />
        </View>
        {/* <TextInput style={styles.input} placeholder="[Search My Messages]" onChangeText={onChangeText} value={text} /> */}

        {/* <TouchableOpacity onPress={() => handleAddToContact()}>
          <View style={styles.addContact}>
            <Image style={styles.tinyLogo} source={require('../../Assets/Images/addcontactplus.png')} />
            <Text style={{...typographyVariant.the32}}>Add to text</Text>
          </View>
        </TouchableOpacity> */}

        {/* <FlatList data={userContacts} renderItem={renderItem} keyExtractor={(item, index) => index.toString()} /> */}
        <FlatList data={TextTabItemsFromLocalJson} renderItem={renderItem} keyExtractor={(item, index) => index.toString()} />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  imageStyle: {
    width: 35,
    height: 35,
    borderRadius: 17,
    paddingRight: 15,
  },
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#00000008',
    // height: 50,
    // flexDirection: 'row',
    // alignItems: 'center',
    backgroundColor: '#fff',
    // padding: 20,
    // marginHorizontal: 16,
  },
  addContact: {
    // height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#00000025',
  },
  title: {
    fontSize: 14,
  },
  tinyLogo: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    marginRight: 10,
  },
  logo: {
    width: 66,
    height: 58,
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    height: 50,
    marginVertical: 12,
    borderWidth: 1,
    borderColor: '#00000025',
    color: '#00000080',
    marginHorizontal: 16,
  },
});
export default TextTab;
