import React, { useState } from 'react';
import { Button, Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Contacts from '../../Store/localData/ContactItems.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { usePubNub } from 'pubnub-react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../Store/Actions/Global';
import { getModalProps } from '../../Store/reduxSelectors';
import ChooseUser from '../Common/ChooseUser';
import IncomingCallAlert from '../Common/IncomingCallAlert';
import { useIsFocused, StackNavigationState } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
const events = {
  incomingCall: 'incomingCall',
  incomingAccepted: 'incomingAccepted',
  incomingDeclined: 'incomingDeclined',
  callCancelled: 'callCancelled',
  groupCall: 'groupCall',
};

const GROUP_CHAT_LIMIT = 3;

const Home = ({ navigation, route }) => {
  const pubnub = usePubNub();
  const isFocused = useIsFocused();
  const isFocusedRef = React.useRef(null);
  isFocusedRef.current = isFocused;
  const dispatch = useDispatch();
  const [currentUser, setCurrentUser] = React.useState('');
  const [channels, setChannels] = React.useState([]);
  const [currentScreen, setCurrentScreen] = React.useState('');
  const currentScreenRef = React.useRef(null);
  currentScreenRef.current = currentScreen;
  const [groupChatUsers, setGroupChatUsers] = React.useState([]);

  const handleOnLongPress = (user) => {
    let _groupChatUsers = [...groupChatUsers];
    let index = _groupChatUsers.findIndex((channelId) => channelId === user.name + user.id);
    if (index > -1) _groupChatUsers.splice(index, 1);
    else {
      const limitReached = _groupChatUsers.length === GROUP_CHAT_LIMIT;
      if (limitReached) alert("Can't Select More than 3 Contacts!");
      else _groupChatUsers.push(user.name + user.id);
    }
    setGroupChatUsers(_groupChatUsers);
  };

  const handleOnSelectUser = (data) => {
    const isLongPressAction = groupChatUsers.length;
    if (isLongPressAction) handleOnLongPress(data);
    else navigation.push('ContactDetails', { data, currentUser });
  };

  const handleChooseUserModal = (isVisible) => {
    dispatch(
      toggleModal(
        isVisible,
        <ChooseUser
          onSelect={(data) => {
            setCurrentUser(data);
            handleSelectUser(data);
          }}
        />,
      ),
    );
  };

  const handleSelectUser = async (user) => {
    await AsyncStorage.setItem('@currentUser', JSON.stringify(user));
    handleChooseUserModal(false);
  };

  useFocusEffect(
    React.useCallback(() => {
      setCurrentScreen(route && route.name);
    }, [navigation]),
  );

  const handleMessage = (event) => {
    // Message Recieving Section
    const payload = JSON.parse(event && event.message);

    let isGroupCall;
    switch (payload.event) {
      case events.incomingCall:
        dispatch(toggleModal(true, <IncomingCallAlert onDecline={(receiverInfo) => handleCallAction(false, receiverInfo)} onAccept={(receiverInfo) => handleCallAction(true, receiverInfo)} data={Contacts.find((contact) => contact.id === payload.from)} />));
        break;
      case events.incomingAccepted:
        if (currentScreenRef.current !== 'ChatScreen') {
          isGroupCall = payload && payload.groupId;
          const currentUserHaveAccess = payload.groupUsers && payload.groupUsers.includes(currentUser.name + currentUser.id);
          const shouldIgnoreThis = payload.senderInfo.id === currentUser.id;

          // if (isFocusedRef.current) {
          if (shouldIgnoreThis) return null;

          if (isGroupCall) {
            if (currentUserHaveAccess) {
              setCurrentScreen('ChatScreen');
              navigation.push('ChatScreen', { receiverInfo: Contacts.filter((contact) => payload.groupUsers.includes(contact.name + contact.id) && contact.id !== (currentUser && currentUser.id)), currentUser, isTextCall: true, groupId: payload && payload.groupId, addInfo: payload });
            }
          } else {
            let receiverInfo = Contacts.find((contact) => contact.id === payload.from);
            setCurrentScreen('ChatScreen');
            navigation.push('ChatScreen', { receiverInfo, currentUser, isTextCall: true });
          }
          setGroupChatUsers([]);
        }

        dispatch(toggleModal(false, null));
        break;
      case events.groupCall:
        dispatch(
          toggleModal(
            true,
            <IncomingCallAlert
              onDecline={(receiverInfo) => handleCallAction(false, receiverInfo, payload)}
              onAccept={(receiverInfo) => handleCallAction(true, receiverInfo, payload)}
              data={Contacts.filter((contact) => payload.groupUsers.includes(contact.name + contact.id) && contact.id !== (currentUser && currentUser.id))}
            />,
          ),
        );
        break;
      case events.callCancelled:
      case events.incomingDeclined:
        if (payload && payload.groupId) return null;
        dispatch(toggleModal(false, null));
        break;
    }
  };

  // Call Accepted Or Rejected

  const handleCallAction = (isAccepted, receiverInfo, addInfo) => {
    const isGroupCall = addInfo && addInfo.groupId;
    let payload = {
      from: currentUser.id,
      to: receiverInfo.name + receiverInfo.id,
      event: isAccepted ? events.incomingAccepted : events.incomingDeclined,
      senderInfo: currentUser,
      groupId: addInfo && addInfo.groupId,
      groupUsers: addInfo && addInfo.groupUsers,
    };
    if (isGroupCall) {
      addInfo.groupUsers.forEach((channelId) => {
        payload.to = channelId;
        pubnub.publish({
          channel: channelId,
          message: JSON.stringify(payload),
        });
      });
      pubnub.publish({
        channel: addInfo && addInfo.groupId,
        message: JSON.stringify(payload),
      });
    } else {
      pubnub.publish({
        channel: receiverInfo.name + receiverInfo.id,
        message: JSON.stringify(payload),
      });
    }
    dispatch(toggleModal(false, null));
    if (isAccepted) {
      setCurrentScreen('ChatScreen');
      navigation.push('ChatScreen', { receiverInfo, currentUser, isTextCall: true, groupId: addInfo && addInfo.groupId, addInfo });
    }
  };

  // Creating Group Call

  const handleGroupCall = () => {
    let payload = {
      from: currentUser.name + currentUser.id,
      event: 'groupCall',
      groupId: currentUser.name + Math.random(),
      groupUsers: groupChatUsers.concat(currentUser.name + currentUser.id),
    };

    groupChatUsers.forEach((channelId) => {
      payload.to = channelId;
      pubnub.publish({
        channel: channelId,
        message: JSON.stringify(payload),
      });
    });
    setGroupChatUsers([]);
    dispatch(toggleModal(true, <IncomingCallAlert isIncomingCall={false} onDecline={(receiverInfo) => handleCallAction(false, receiverInfo)} onAccept={(receiverInfo) => handleCallAction(true, receiverInfo)} data={Contacts.filter((contact) => groupChatUsers.includes(contact.name + contact.id))} />));
  };
  React.useEffect(() => {
    if (channels.length) {
      pubnub.addListener({ message: handleMessage });
      pubnub.subscribe({ channels });
    }

    return () => {
      removeAllPubnub();
    };
  }, [pubnub, channels]);

  React.useEffect(() => {
    _componentDidMount();
    return () => {
      removeAllPubnub();
    };
  }, []);

  const removeAllPubnub = () => {
    pubnub.unsubscribeAll();
    pubnub.removeListener();
  };

  React.useEffect(() => {
    if (currentUser && currentUser.name) setChannels([...new Set([...channels, `${currentUser.name + currentUser.id}`])]);
  }, [currentUser]);

  const _componentDidMount = async () => {
    checkIfUserLoggedIn();
  };
  const checkIfUserLoggedIn = async () => {
    const hasUser = await AsyncStorage.getItem('@currentUser');
    if (hasUser) setCurrentUser(JSON.parse(hasUser));
    else handleChooseUserModal(true);
  };

  const handleLogout = async () => {
    await AsyncStorage.removeItem('@currentUser');
    setCurrentUser({});
    setTimeout(() => {
      checkIfUserLoggedIn();
    });
  };

  const handleContacts = () => {
    navigation.push('Registration');
  };

  React.useLayoutEffect(() => {
    if (currentUser && currentUser.name) {
      navigation.setOptions({
        headerTitle: () => (
          <Text>
            <Text style={{ fontSize: 20 }}>Welcome </Text>
            <Text style={{ fontSize: 20, ...typographyWeight.bold, }}>{currentUser.name}</Text>
          </Text>
        ),
        headerRight: () => (
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={() => handleContacts()}>
              <Text style={{ fontSize: 15, ...typographyWeight.bold, }}>Registration</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={() => handleLogout()}>
              <Text style={{ fontSize: 15, ...typographyWeight.bold, }}>Logout</Text>
            </TouchableOpacity>
          </View>
        ),
      });
    }
  }, [navigation, currentUser]);

  return (
    <View>
      {groupChatUsers.length > 1 ? (
        <View style={{ height: 50, justifyContent: 'center' }}>
          <TouchableOpacity style={{ paddingHorizontal: 20, alignSelf: 'flex-end' }} onPress={() => handleGroupCall()}>
            <Text style={{ fontSize: 15, ...typographyWeight.bold, }}>Call</Text>
          </TouchableOpacity>
        </View>
      ) : null}

      <View>
        {Contacts.filter((contact) => contact.id !== (currentUser && currentUser.id)).map((item, index) => (
          <TouchableOpacity
            key={index}
            style={{ backgroundColor: groupChatUsers.includes(item.name + item.id) ? '#00000025' : '#fff', padding: 10, borderBottomWidth: 1, borderBottomColor: '#00000010', flexDirection: 'row', alignItems: 'center' }}
            onLongPress={() => handleOnLongPress(item)}
            onPress={() => handleOnSelectUser(item)}>
            <Image resizeMode="cover" style={styles.tinyLogo} source={{ uri: item.profilePic }} />
            <Text style={{ marginLeft: 10 }}>{item.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  logo: {
    width: 66,
    height: 58,
  },
});
export default Home;
