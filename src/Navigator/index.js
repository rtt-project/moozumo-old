import React, {useState} from 'react';

import {NavigationContainer, getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {TouchableHighlight, SafeAreaView, Text, Image, Button, View, ActivityIndicator, TouchableOpacity, ImageBackground, StatusBar} from 'react-native';
import {useKeyboard, useDimensions} from '@react-native-community/hooks';
import VersionInfo from 'react-native-version-info';
import {useDispatch} from 'react-redux';

// import {Host} from 'react-native-portalize';
// import Home from '../Components/Home';
import ContactList from '../Components/ContactList/index.tsx';
import Registration from '../Components/Registration';
import VerifyOtp from '../Components/Registration/VerifyOTP';
import ContactDetails from '../Components/ContactDetails';
import {useSelector} from 'react-redux';
// import ContactDetails from '../Components/ContactDetails';
import Chat from '../Components/Chat';
import Call from '../Components/Call';
import IncomingCallScreen from '../Components/Call/IncomingCallScreen';
import LinearGradient from 'react-native-linear-gradient';
import {getUserFetchState, getUserInfo} from '../Store/reduxSelectors';
import CreateContact from '../Components/CreateContact';
import {typographyVariant, typographyWeight} from '../Constants/typography';
import CustomButton from '../Components/Common/CustomButton';
import TextTab from '../Components/TextTab';
import MessageTab from '../Components/MessageTab';
import Stepper from '../Components/Common/Stepper';
import {colors} from '../Constants/colors';
import {checkPermissionToReadContact} from '../Store/Actions/Contacts';
import useCallEvents from '../Helper/useCallEvents';

const PublicStack = createStackNavigator();
const PrivateStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const PublicRoutes = () => (
  <PublicStack.Navigator>
    <PublicStack.Screen name="Registration" component={Registration} options={{headerShown: false}} />
    <PublicStack.Screen name="VerifyOtp" component={VerifyOtp} options={{headerShown: false}} />
  </PublicStack.Navigator>
);

function PrivateRouteWithTabs() {
  const [] = useCallEvents();

  return (
    <Tab.Navigator
      initialRouteName="Contact"
      backBehavior="none"
      tabBarOptions={{
        activeTintColor: colors.cornFlowerBlue,
        inactiveTintColor: colors.black,
      }}>
      <Tab.Screen
        options={props => {
          return {
            tabBarIcon: ({color, size}) => <Image style={{width: 15, height: 15, tintColor: color}} source={require('../Assets/Images/message_icon.png')} />,
          };
        }}
        name="Message"
        component={MessageTab}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({color, size}) => <Image style={{width: 15, height: 15, tintColor: color}} source={require('../Assets/Images/text_call.png')} />,
        }}
        name="Text Call"
        component={TextTab}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({color, size}) => <Image style={{width: 20, height: 15, tintColor: color}} source={require('../Assets/Images/addcontact.png')} />,
        }}
        name="Contact"
        component={ContactList}
      />
    </Tab.Navigator>
  );
}
//             <Text style={{...typographyVariant.the20, ...typographyWeight.bold}}>Message</Text>

const PrivateRoutes = () => (
  <PrivateStack.Navigator>
    {/* 1st item loads on login */}
    {/* Contact List */}

    <PrivateStack.Screen
      options={({route}) => {
        const routeName = getFocusedRouteNameFromRoute(route);

        return {
          // headerShown: false,
          headerBackground: () => <LinearGradient colors={[colors.headerbgstart, '#fff']} style={{flex: 1}} />,
          headerTitle: routeName || 'Contact',
          headerTitleStyle: {
            ...typographyVariant.the20,
            ...typographyWeight.bold,
          },
          headerRight: () => (
            <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15}}>
              <Image style={{width: 4.5, height: 18}} source={require('../Assets/Images/threedots.png')} />
            </View>
          ),
        };
      }}
      name="Home"
      component={PrivateRouteWithTabs}
    />

    <PrivateStack.Screen
      options={{
        headerBackground: () => <LinearGradient colors={[colors.headerbgstart, '#fff']} style={{flex: 1}} />,
        headerTitle: 'New Contact',
        headerTitleStyle: {
          ...typographyVariant.the20,
          ...typographyWeight.bold,
        },
      }}
      name="CreateContact"
      component={CreateContact}
    />
    <PrivateStack.Screen
      options={({route: {params}}) => ({
        headerBackground: () => <LinearGradient colors={[colors.headerbgstart, '#fff']} style={{flex: 1}} />,
        headerTitle: params && params.receiverInfo.name,
        headerTitleStyle: {
          ...typographyVariant.the20,
          ...typographyWeight.bold,
        },
      })}
      name="ChatScreen"
      component={Chat}
    />
    <PrivateStack.Screen
      options={{
        headerShown: false,
      }}
      name="CallSCreen"
      component={Call}
    />
    <PrivateStack.Screen
      options={{
        headerShown: false,
      }}
      name="IncomingCallSCreen"
      component={IncomingCallScreen}
    />
    {/* <PrivateStack.Screen name="Home" component={Home} />
    <PrivateStack.Screen name="Header" component={ContactScreen} />
 
    <PrivateStack.Screen
      name="ChatScreen"
      component={Chat}
      options={{
        headerTitle: false,
        headerBackground: () => <LinearGradient colors={['#9ce0fb', '#fff']} style={{flex: 1}} />,
        headerTitleStyle: {color: '#000000'},
      }}
    /> */}
    <PrivateStack.Screen
      name="ContactDetails"
      component={ContactDetails}
      options={{
        headerBackground: () => <LinearGradient colors={[colors.headerbgstart, colors.contacttopbggrey]} style={{flex: 1}} />,
        headerTitle: '',
        headerTitleStyle: {
          ...typographyVariant.the20,
          ...typographyWeight.bold,
        },
        headerRight: () => (
          <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15}}>
            <Image style={{width: 4.5, height: 18}} source={require('../Assets/Images/threedots.png')} />
          </View>
        ),
      }}
    />
  </PrivateStack.Navigator>
);

function Navigator() {
  const dispatch = useDispatch();
  const userInfo = useSelector(state => getUserInfo(state));
  const isUserInfoFetching = useSelector(state => getUserFetchState(state));
  const dimensions = useDimensions().screen;

  React.useEffect(() => {
    // dispatch(checkPermissionToReadContact());
  }, []);

  return (
    <NavigationContainer>
      {isUserInfoFetching ? (
        <ImageBackground style={{flex: 1, resizeMode: 'cover', justifyContent: 'center'}} source={require('../Assets/Images/splashscreenwithoutlogo.jpg')}>
          <StatusBar translucent backgroundColor="transparent" />
          <View style={{flex: 1, alignContent: 'space-between', justifyContent: 'space-between'}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Image
                style={{
                  width: dimensions.width * 0.55 * 0.8,
                  height: dimensions.width * 0.497 * 0.8,
                  marginTop: dimensions.height * 0.133,
                }}
                source={require('../Assets/Images/logo_only.png')}
              />
              <ActivityIndicator></ActivityIndicator>
              {/* <Button onPress={requsetPermission} title="Load Contacts" accessibilityLabel="Please grant access to read contacts" /> */}
            </View>
            <SafeAreaView style={{justifyContent: 'flex-end', alignItems: 'center'}}>
              <Text style={{...typographyVariant.the13, color: colors.contacttopbggrey}}>{`Version ${VersionInfo.appVersion} - Prototype`}</Text>
            </SafeAreaView>
          </View>
        </ImageBackground>
      ) : userInfo && userInfo.userId ? (
        <PrivateRoutes />
      ) : (
        <PublicRoutes />
      )}
    </NavigationContainer>
  );
}

export default Navigator;
