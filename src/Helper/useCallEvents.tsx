import React from 'react';
import {usePubNub} from 'pubnub-react';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import messageEvents from '../Constants/messageEvents';
import {messagePayload, senderInfoType} from '../Constants/types';
import IncomingCallScreen from '../Components/Call/IncomingCallScreen';
import {getUserContactsFromState, getUserInfo} from '../Store/reduxSelectors';
import {toggleModal} from '../Store/Actions/Global';
import useCallActions from './useCallActions';
import {vibrate} from '../Store/Services/Vibration';

const CALL_ACTION_TYPES = {
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_REJECTED: 'CALL_REJECTED',
};

const useCallEvents = (): [] => {
  const pubnub = usePubNub();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [callActions] = useCallActions();
  const userContacts = useSelector(state => getUserContactsFromState(state));
  const userContactsRef = React.useRef([]);
  userContactsRef.current = userContacts;

  const handleMessage = (event: any) => {
    try {
      const payload: messagePayload = JSON.parse(event && event.message);
      let senderInfo;
      switch (payload.event) {
        case messageEvents.incomingCall:
          vibrate('start');
          senderInfo = getSenderInfo(payload);
          if (senderInfo && senderInfo.userId) dispatch(toggleModal(true, <IncomingCallScreen onDecline={(receiverInfo: senderInfoType) => callActions.handleCallAction(CALL_ACTION_TYPES.CALL_REJECTED, receiverInfo)} onAccept={(receiverInfo: senderInfoType) => callActions.handleCallAction(CALL_ACTION_TYPES.CALL_ACCEPTED, receiverInfo)} data={senderInfo} />));
          break;
        case messageEvents.callCancelled:
          vibrate('stop');
          dispatch(toggleModal(false, null));
          break;
        case messageEvents.callAccepted:
          senderInfo = getSenderInfo(payload);
          if (senderInfo && senderInfo.userId) {
            callActions.redirectToChatScreen(senderInfo, true);
            dispatch(toggleModal(false, null));
          }
          break;
      }
    } catch (err) {}
  };

  const getSenderInfo = (payload: messagePayload): senderInfoType => {
    const senderId = payload && payload.metadata && payload.metadata.senderInfo?.userId;
    let senderInfo = userContactsRef.current.find((contact: {userId: string}) => contact.userId === senderId) || {};
    senderInfo = {...(payload && payload.metadata && payload.metadata.senderInfo), ...senderInfo};
    return senderInfo;
  };

  React.useEffect(() => {
    pubnub.addListener({message: handleMessage});

    return () => {
      pubnub.removeListener();
    };
  }, [pubnub]);

  return [];
};

export default useCallEvents;
