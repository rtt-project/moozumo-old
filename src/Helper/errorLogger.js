import {logger, fileAsyncTransport} from 'react-native-logs';
import RNFS from 'react-native-fs';
/* EXPO:
 * import * as FileSystem from 'expo-file-system';
 */

let today = new Date();
let date = today.getDate();
let month = today.getMonth() + 1;
let year = today.getFullYear();

const config = {
  severity: 'debug',
  transport: fileAsyncTransport,
  transportOptions: {
    FS: RNFS,
    /* EXPO:
     * FS: FileSystem,
     */
    fileName: `logs_${date}-${month}-${year}`, // Create a new file every day
  },
};

var errorLogger = logger.createLogger(config);

export default errorLogger;
