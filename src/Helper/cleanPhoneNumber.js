import PhoneNumber from 'awesome-phonenumber';

const cleanPhoneNumber = number => {
  let phoneNumber = number.replace(/[()-]/g, '');
  let pn = PhoneNumber(phoneNumber);
  phoneNumber = pn.getNumber('significant') || phoneNumber;
  phoneNumber = phoneNumber.split(' ').join('');
  return phoneNumber;
};
export default cleanPhoneNumber;
