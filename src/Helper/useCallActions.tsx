import React from 'react';
import {usePubNub} from 'pubnub-react';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import messageEvents from '../Constants/messageEvents';
import {messagePayload, senderInfoType, useCallActionsArgumentTypes} from '../Constants/types';
import IncomingCallAlert from '../Components/Common/IncomingCallAlert';
import {getUserInfo} from '../Store/reduxSelectors';
import {toggleModal} from '../Store/Actions/Global';
import {vibrate} from '../Store/Services/Vibration';

const CALL_ACTION_TYPES = {
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_REJECTED: 'CALL_REJECTED',
};

const useCallActions = (): useCallActionsArgumentTypes => {
  const pubnub = usePubNub();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const userInfo = useSelector(state => getUserInfo(state));

  const handleCallAction = (callActionType: string, senderInfo: senderInfoType) => {
    if (senderInfo.userId) {
      let payload: messagePayload = {
        channelId: senderInfo.userId,
        from: userInfo.userId,
        to: senderInfo.userId,
        event: messageEvents.callCancelled,
        metadata: {
          senderInfo: userInfo,
        },
      };
      switch (callActionType) {
        case CALL_ACTION_TYPES.CALL_REJECTED:
          payload.event = messageEvents.callCancelled;
          messageThisUser(payload);
          break;
        case CALL_ACTION_TYPES.CALL_ACCEPTED:
          payload.event = messageEvents.callAccepted;
          messageThisUser(payload);
          redirectToChatScreen(senderInfo, true);
      }
      dispatch(toggleModal(false, null));
    } else {
    }

    vibrate('stop');
  };

  const redirectToChatScreen = (receiverInfo: senderInfoType, isChat?: boolean) => navigation.navigate('ChatScreen', {receiverInfo, isChat});

  const callThisUser = (payload: messagePayload, senderInfo: senderInfoType) => {
    messageThisUser(payload);
    dispatch(toggleModal(true, <IncomingCallAlert data={senderInfo} isIncomingCall={false} onDecline={(payload: senderInfoType) => handleCallAction(CALL_ACTION_TYPES.CALL_REJECTED, payload)} />));
  };

  const messageThisUser = (payload: messagePayload) => {
    pubnub.publish({
      channel: payload.channelId,
      message: JSON.stringify(payload),
    });
  };

  return [{redirectToChatScreen, callThisUser, messageThisUser, handleCallAction}];
};

export default useCallActions;
